'use strict';
var File = require('dw/io/File');
var FileReader = require('dw/io/FileReader');
var FileWriter = require('dw/io/FileWriter');
var CSVStreamWriter = require('dw/io/CSVStreamWriter');
var CustomObjectMgr = require('dw/object/CustomObjectMgr');
var ArrayList = require('dw/util/ArrayList');
var Transaction = require('dw/system/Transaction');
var myObject = [];
var csvHeader = [];
var csvDataAll = [];
var exportCustomObject = CustomObjectMgr.getCustomObject('catalogCSVImpex', 'catalogImpexCSV');
var catalogFile = exportCustomObject.custom.inputFile;
var outputFilePath = exportCustomObject.custom.outputFile;
var catalogAttributes = exportCustomObject.custom.catalogAttributes ? exportCustomObject.custom.catalogAttributes.split(',') : null;
var imageColumns = [];


/**
 * getChildNodes returns xml childs of the arg xml object
 * @param {Object} xmlObject XML Object to get child nodes of
 * @returns {Object} object children
 */
function getChildNodes(xmlObject) {
    var myObjectChildren = [];
    var children = xmlObject.children();
    for (var index = 0; index < children.length(); index++) {
        if (children[index].toString().trim() !== '') {
            myObjectChildren.push(children[index]);
        } else if (children[index].attributes().toString().trim() !== '') {
            myObjectChildren.push(children[index]);
        }
    }
    return myObjectChildren;
}

module.exports = {
    beforeStep: function () {
        var file = new File(catalogFile);
        var xmlObject = new ArrayList([]);
        if (file.exists()) {
            // Test start
            var fileReader = new FileReader(file);
            var XMLStreamReader = require('dw/io/XMLStreamReader');
            var xmlReader = new XMLStreamReader(fileReader);
            while (xmlReader.hasNext()) {
                try {
                    if (xmlReader.next()) {
                        if (xmlReader.isStartElement()) {
                            var localElementName = xmlReader.getLocalName();
                            if (localElementName === 'product' || localElementName === 'category-assignment') {
                                xmlObject.push(xmlReader.readXMLObject());
                            }
                        }
                    }
                } catch (error) {
                    continue;
                }
            }
        }
        myObject = xmlObject.iterator();
    },
    read: function () {
        if (myObject.hasNext()) {
            return myObject.next();
        }
    },
    process: function (myObjects) {
        var csvObject = {};
        csvObject['product-id'] = myObjects.attribute('product-id').toString();
        if (!csvHeader.includes('product-id')) {
            csvHeader.push('product-id');
        }
        csvObject['category-id'] = myObjects.attribute('category-id').toString();
        if (!csvHeader.includes('category-id')) {
            csvHeader.push('category-id');
        }
        var myObjectChildren = getChildNodes(myObjects);
        myObjectChildren.forEach(function (myObjectChild) {
            var attributeName = myObjectChild.localName();
            if (catalogAttributes.length === 1 || catalogAttributes.includes(attributeName)) {
                if (attributeName === 'images') {
                    var imageGroups = getChildNodes(myObjectChild);
                    imageGroups.forEach(function (imageGroup) {
                        var viewType = imageGroup.attribute('view-type').toString();
                        var variationValue = imageGroup.attribute('variation-value');
                        if (!variationValue == null) {
                            viewType += '-' + variationValue;
                        }
                        var variations = [];
                        var imagePath = '';
                        var imageElements = getChildNodes(imageGroup);
                        imageElements.forEach(function (element) {
                            if (element.localName() === 'variation') {
                                var attributeId = element.attribute('attribute-id').toString();
                                var value = element.attribute('value').toString();
                                variations.push(attributeId + '-' + value);
                            } else if (element.localName() === 'image') {
                                imagePath = element.attribute('path').toString();
                            }
                        });
    
                        var key = 'image-' + viewType + (variations.length > 0 ? '-' + variations.join('-') : '');
                        if (!csvHeader.includes(key)) {
                            csvHeader.push(key);
                        }
                        csvObject[key] = imagePath;
                    });
                } else if (attributeName === 'custom-attributes') {
                    var customAttributesChildren = getChildNodes(myObjectChild);
                    customAttributesChildren.forEach(function (customAttribute) {
                        var xmlString = customAttribute.toXMLString();
                        var langAttributeStart = 0;
                        var langAttributeEnd = 0;
                        var langAttribute = null;
                        if (xmlString.indexOf('xml:lang="') !== -1) {
                            langAttributeStart = xmlString.indexOf('xml:lang="') + 'xml:lang="'.length;
                            langAttributeEnd = xmlString.indexOf('"', langAttributeStart);
                            langAttribute = xmlString.substring(langAttributeStart, langAttributeEnd);
                        }
                        var key;
                        if (langAttribute) {
                            key = 'c_' + customAttribute.attribute("attribute-id") + ':' + langAttribute;
                        } else {
                            key = 'c_' + customAttribute.attribute("attribute-id");
                        }
                        if (typeof customAttribute.children() === typeof customAttribute && getChildNodes(customAttribute.children()).length > 1) {
                            var values = getChildNodes(customAttribute.children());
                            csvObject[key] = values.join('|');
                        } else {
                            csvObject[key] = customAttribute.children();
                        }
                        if (!csvHeader.includes(key)) {
                            csvHeader.push(key);
                        }
                    });
                } else if (attributeName === 'store-attributes') {
                    var storeAttributesChildren = getChildNodes(myObjectChild);
                    storeAttributesChildren.forEach(function (storeAttribute) {
                        var xmlString = storeAttribute.toXMLString();
                        var langAttributeStart = 0;
                        var langAttributeEnd = 0;
                        var langAttribute = null;
                        if (xmlString.indexOf('xml:lang="') !== -1) {
                            langAttributeStart = xmlString.indexOf('xml:lang="') + 'xml:lang="'.length;
                            langAttributeEnd = xmlString.indexOf('"', langAttributeStart);
                            langAttribute = xmlString.substring(langAttributeStart, langAttributeEnd);
                        }
                        var key;
                        if (langAttribute) {
                            key = 's_' + storeAttribute.localName() + ':' + langAttribute;
                        } else {
                            key = 's_' + storeAttribute.localName();
                        }
                        csvObject[key] = storeAttribute.children();
                        if (!csvHeader.includes(key)) {
                            csvHeader.push(key);
                        }
                    });
                } else if (attributeName === 'variations') {
                    var variation = getChildNodes(myObjectChild);
                    if (variation[0].localName() === 'attributes') {
                        var variationAttributes = getChildNodes(variation[0]);
                        variationAttributes.forEach(function (variationAttribute) {
                            var key = 'v_' + variationAttribute.attribute('variation-attribute-id');
                            var valuesNames = getChildNodes(variationAttribute.children('variation-attribute-values'));
                            var valuesNamesArray = [];
                            valuesNames.forEach(function (valueName) {
                                if (valueName.localName() === 'variation-attribute-value') {
                                    valuesNamesArray.push(valueName.attribute('value'));
                                }
                            });
                            if (!csvHeader.includes(key)) {
                                csvHeader.push(key);
                            }
                            csvObject[key] = valuesNamesArray;
                        });
                        if (variation[1] && variation[1].localName() === 'variants') {
                            var variants = getChildNodes(variation[1]);
                            if (!csvHeader.includes('v_variants')) {
                                csvHeader.push('v_variants');
                            }
                            var variantsArray = [];
                            variants.forEach(function (variant) {
                                variantsArray.push(variant.attribute('product-id'));
                            });
                            csvObject['v_variants'] = variantsArray;
                        }
                    }
                } else if (attributeName === 'category-assignment') {
                    csvObject['category-id'] = myObjects.attribute('category-id').toString();
                    csvObject['product-id'] = myObjects.attribute('product-id').toString();

                    if (!csvHeader.includes('category-id')) {
                        csvHeader.push('category-id');
                    }
                    if (!csvHeader.includes('product-id')) {
                        csvHeader.push('product-id');
                    }
                } else {
                    var xmlString = myObjectChild.toXMLString();
                    var langAttributeStart = 0;
                    var langAttributeEnd = 0;
                    var langAttribute = null;
                    if (xmlString.indexOf('xml:lang="') !== -1) {
                        langAttributeStart = xmlString.indexOf('xml:lang="') + 'xml:lang="'.length;
                        langAttributeEnd = xmlString.indexOf('"', langAttributeStart);
                        langAttribute = xmlString.substring(langAttributeStart, langAttributeEnd);
                    }
                    var key;
                    if (langAttribute) {
                        key = myObjectChild.localName() + ':' + langAttribute;
                    } else {
                        key = myObjectChild.localName();
                    }
                    csvObject[key] = myObjectChild.children() ? myObjectChild.children() : '';
                    if (!csvHeader.includes(key)) {
                        csvHeader.push(key);
                    }
                }
            }
        });
        return csvObject;
    },
    write: function (csvObjectsList) {
        var csvObjectsIterator = csvObjectsList.iterator();
        while (csvObjectsIterator.hasNext()) {
            var currentObject = csvObjectsIterator.next();
            if (currentObject) {
                var csvData = [];
                csvHeader.forEach(function (prop) {
                    csvData.push(currentObject[prop] ? currentObject[prop] : '');
                });
                csvDataAll.push(csvData);
            }
        }
    },
    afterStep: function () {
        var outputFile = new File(outputFilePath);
        var csvFileWriter = new FileWriter(outputFile);
        var csvStreamWriter = new CSVStreamWriter(csvFileWriter, ',');
        csvStreamWriter.writeNext(csvHeader);
        csvDataAll.forEach(function (csvData) {
            csvStreamWriter.writeNext(csvData);
        });
        csvStreamWriter.close();
        csvFileWriter.close();
        var lastFiles = 'lastFiles' in exportCustomObject.custom && exportCustomObject.custom.lastFiles ? JSON.parse(exportCustomObject.custom.lastFiles) : [];
        var latestFile = 'latestImpExFile' in exportCustomObject.custom && exportCustomObject.custom.latestImpExFile ? JSON.parse('[' + exportCustomObject.custom.latestImpExFile + ']') : {};
        latestFile[0].Status = 'success';
        lastFiles.unshift(latestFile[0]);
        if (lastFiles.length > 5) {
            lastFiles.pop();
        }
        Transaction.wrap(function () {
            exportCustomObject.custom.latestImpExFile = '';
            exportCustomObject.custom.lastFiles = JSON.stringify(lastFiles);
        });
    }
};

