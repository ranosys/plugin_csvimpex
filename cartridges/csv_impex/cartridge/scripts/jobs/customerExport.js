'use strict';
var File = require('dw/io/File');
var FileReader = require('dw/io/FileReader');
var FileWriter = require('dw/io/FileWriter');
var CSVStreamWriter = require('dw/io/CSVStreamWriter');
var CustomObjectMgr = require('dw/object/CustomObjectMgr');
var ArrayList = require('dw/util/ArrayList');
var Transaction = require('dw/system/Transaction');

var myObject = [];
var csvHeader = [];
var csvDataAll = [];

var exportCustomObject = CustomObjectMgr.getCustomObject('customerCSVImpex', 'customerImpexCSV');
var customerFile = exportCustomObject.custom.inputFile;
var outputFilePath = exportCustomObject.custom.outputFile;

function getChildNodes(xmlObject) {
    var myObjectChildren = [];
    var children = xmlObject.children();
    for (var index = 0; index < children.length(); index++) {
        if (children[index].toString().trim() !== '') {
            myObjectChildren.push(children[index]);
        } else if (children[index].attributes().toString().trim() !== '') {
            myObjectChildren.push(children[index]);
        }
    }
    return myObjectChildren;
}

module.exports = {
    beforeStep: function () {
        var file = new File(customerFile);
        var xmlObject = new ArrayList([]);
        if (file.exists()) {
            var fileReader = new FileReader(file);
            var XMLStreamReader = require('dw/io/XMLStreamReader');
            var xmlReader = new XMLStreamReader(fileReader);
            while (xmlReader.hasNext()) {
                try {
                    if (xmlReader.next()) {
                        if (xmlReader.isStartElement()) {
                            var localElementName = xmlReader.getLocalName();
                            if (localElementName === 'customer') {
                                xmlObject.push(xmlReader.readXMLObject());
                            }
                        }
                    }
                } catch (error) {
                    continue;
                }
            }
        }
        myObject = xmlObject.iterator();
    },
    read: function () {
        if (myObject.hasNext()) {
            return myObject.next();
        }
    },
    process: function (myObjects) {
        var csvObject = {};
        csvObject['customer-no'] = myObjects.attribute('customer-no').toString();
        if (!csvHeader.includes('customer-no')) {
            csvHeader.push('customer-no');
        }
        var myObjectChildren = getChildNodes(myObjects);
        myObjectChildren.forEach(function (myObjectChild) {
            if (myObjectChild.localName() === 'credentials') {
                var credentialsChildren = getChildNodes(myObjectChild);
                credentialsChildren.forEach(function (credential) {
                    var key = 'credentials_' + credential.localName();
                    csvObject[key] = credential.children();
                    if (!csvHeader.includes(key)) {
                        csvHeader.push(key);
                    }
                });
            } else if (myObjectChild.localName() === 'profile') {
                var profileChildren = getChildNodes(myObjectChild);
                profileChildren.forEach(function (profile) {
                    if (profile.localName() === 'custom-attributes') {
                        var customAttributes = getChildNodes(profile);
                        customAttributes.forEach(function (customAttribute) {
                            var key = 'c_' + customAttribute.attribute('attribute-id').toString();
                            csvObject[key] = customAttribute.children().toString();
                            if (!csvHeader.includes(key)) {
                                csvHeader.push(key);
                            }
                        });
                    } else {
                        var key = 'profile_' + profile.localName();
                        csvObject[key] = profile.children().toString();
                        if (!csvHeader.includes(key)) {
                            csvHeader.push(key);
                        }
                    }
                });
            } else if (myObjectChild.localName() === 'addresses') {
                var addresses = getChildNodes(myObjectChild);
                addresses.forEach(function (address) {
                    var addressIdKey = 'address-id';
                    var preferredKey = 'address-preferred';
                    csvObject[addressIdKey] = address.attribute('address-id').toString();
                    csvObject[preferredKey] = address.attribute('preferred').toString();
                    if (!csvHeader.includes(addressIdKey)) {
                        csvHeader.push(addressIdKey);
                    }
                    if (!csvHeader.includes(preferredKey)) {
                        csvHeader.push(preferredKey);
                    }
                    var addressChildren = getChildNodes(address);
                    addressChildren.forEach(function (addressChild) {
                        var key = 'address_' + addressChild.localName();
                        csvObject[key] = addressChild.children();
                        if (!csvHeader.includes(key)) {
                            csvHeader.push(key);
                        }
                    });
                });
            } else {
                var key = myObjectChild.localName();
                csvObject[key] = myObjectChild.children() ? myObjectChild.children() : '';
                if (!csvHeader.includes(key)) {
                    csvHeader.push(key);
                }
            }
        });
        return csvObject;
    },
    write: function (csvObjectsList) {
        var csvObjectsIterator = csvObjectsList.iterator();
        while (csvObjectsIterator.hasNext()) {
            var currentObject = csvObjectsIterator.next();
            if (currentObject) {
                var csvData = [];
                csvHeader.forEach(function (prop) {
                    csvData.push(currentObject[prop] ? currentObject[prop] : '');
                });
                csvDataAll.push(csvData);
            }
        }
    },
    afterStep: function () {
        var outputFile = new File(outputFilePath);
        var csvFileWriter = new FileWriter(outputFile);
        var csvStreamWriter = new CSVStreamWriter(csvFileWriter, ',');
        csvStreamWriter.writeNext(csvHeader);
        csvDataAll.forEach(function (csvData) {
            csvStreamWriter.writeNext(csvData);
        });
        csvStreamWriter.close();
        csvFileWriter.close();
        var lastFiles = 'lastFiles' in exportCustomObject.custom && exportCustomObject.custom.lastFiles ? JSON.parse(exportCustomObject.custom.lastFiles) : [];
        var latestFile = 'latestImpExFile' in exportCustomObject.custom && exportCustomObject.custom.latestImpExFile ? JSON.parse('[' + exportCustomObject.custom.latestImpExFile + ']') : {};
        latestFile[0].Status = 'success';
        lastFiles.unshift(latestFile[0]);
        if (lastFiles.length > 5) {
            lastFiles.pop();
        }
        Transaction.wrap(function () {
            exportCustomObject.custom.latestImpExFile = '';
            exportCustomObject.custom.lastFiles = JSON.stringify(lastFiles);
        });
    }
};
