'use strict';

var FileReader = require('dw/io/FileReader');
var FileWriter = require('dw/io/FileWriter');
var XMLStreamWriter = require('dw/io/XMLStreamWriter');
var CSVStreamReader = require('dw/io/CSVStreamReader');
var CustomObjectMgr = require('dw/object/CustomObjectMgr');
var StringUtils = require('dw/util/StringUtils');
var Transaction = require('dw/system/Transaction');
var File = require('dw/io/File');
var Pipelet = require('dw/system/Pipelet');
var importCustomObject = CustomObjectMgr.getCustomObject('catalogCSVImpex', 'catalogImpexCSV');
var catalogID = importCustomObject.custom.catalogID;
var CSVFile = importCustomObject.custom.inputFile;
var XMLFile = importCustomObject.custom.outputFile;
var importModeType = importCustomObject.custom.importModeType;
var customAttributeIndexs = [];
var systemAttributeIndexs = [];
var storeAttributeIndexs = [];
var imageIndexs = [];
var variationIndexs = [];
var categoryIndexs = [];
var primaryFlagIndexs = [];
var csvIterator;
var csvHeader;
var xmlFile = new File(XMLFile);
var fileWriter = new FileWriter(xmlFile);
var xsw = new XMLStreamWriter(fileWriter);
var file = new File(CSVFile);
var fileName = file.name.replace('.csv', '.xml');
var fileReader = new FileReader(file);
var csvReader = new CSVStreamReader(fileReader);
var write = 0;
var blankValue;

module.exports = {
    beforeStep: function () {
        var content = csvReader.readAll();
        content[0].forEach(function (elem, elemIndex) {
            if (elem.startsWith('c_', 0)) {
                customAttributeIndexs.push(elemIndex);
            } else if (elem.startsWith('image-', 0)) {
                imageIndexs.push(elemIndex);
            } else if (elem.startsWith('s_', 0)) {
                storeAttributeIndexs.push(elemIndex);
            } else if (elem.startsWith('v_', 0)) {
                variationIndexs.push(elemIndex);
            } else if (elem.startsWith('category-', 0)) {
                categoryIndexs.push(elemIndex);
            } else if (elem.startsWith('primary-', 0)) {
                primaryFlagIndexs.push(elemIndex);
            } else {
                systemAttributeIndexs.push(elemIndex);
            }
        });
        csvHeader = content[0];
        content.shift();
        csvIterator = content.iterator();
        xsw.writeStartDocument();
        xsw.writeStartElement("catalog");
        xsw.writeAttribute('xmlns', "http://www.demandware.com/xml/impex/catalog/2006-10-31");
        xsw.writeAttribute('catalog-id', catalogID);
    },
    read: function () {
        if (csvIterator.hasNext()) {
            return csvIterator.next();
        }
    },
    process: function (myObjects) {
        xsw.writeStartElement("product");
        xsw.writeAttribute('product-id', myObjects[0]);
        if (systemAttributeIndexs.length > 0) {
            systemAttributeIndexs.forEach(function (systemAttributeIndex) {
                var systemAttribute = csvHeader[systemAttributeIndex];
                var systemAttributeSplit = systemAttribute.split(':');
                blankValue = myObjects[systemAttributeIndex] || '';
                var systemAttributevalues;
                if (blankValue.trim()) {
                    xsw.writeStartElement(systemAttributeSplit[0]);
                    if (systemAttributeSplit.length === 2) {
                        xsw.writeAttribute('xml:lang', systemAttributeSplit[1]);
                    }
                    if (myObjects[systemAttributeIndex] && myObjects[systemAttributeIndex].split('|').length > 1) {
                        systemAttributevalues = myObjects[systemAttributeIndex].split('|');
                        systemAttributevalues.forEach(function (value) {
                            xsw.writeStartElement('value');
                            xsw.writeCharacters(StringUtils.encodeString(value, 0).replace(/&amp;/g, "&"));
                            xsw.writeEndElement();
                        });
                    } else {
                        xsw.writeCharacters(myObjects[systemAttributeIndex] ? StringUtils.encodeString(myObjects[systemAttributeIndex], 0).replace(/&amp;/g, "&") : '');
                    }
                    xsw.writeEndElement();
                }
            });
        }
        if (imageIndexs.length > 0) {
            xsw.writeStartElement("images");
            var imageGroups = {};
            imageIndexs.forEach(function (imageIndex) {
                var imageTag = csvHeader[imageIndex].split('-');
                var viewType = imageTag[1];
                var variation = imageTag[2];
                var variationValue = imageTag[3];
                var imagePath = myObjects[imageIndex] || '';
                if (imagePath) {
                    if (!imageGroups[viewType]) {
                        imageGroups[viewType] = [];
                    }
                    imageGroups[viewType].push({
                        path: myObjects[imageIndex] || '',
                        variation: variation,
                        variationValue: variationValue
                    });
                }
            });
            Object.keys(imageGroups).forEach(function (viewType) {
                imageGroups[viewType].forEach(function (image) {
                    xsw.writeStartElement("image-group");
                    xsw.writeAttribute('view-type', viewType);
        
                    if (image.variation && image.variationValue) {
                        xsw.writeStartElement("variation");
                        xsw.writeAttribute('attribute-id', image.variation);
                        xsw.writeAttribute('value', image.variationValue);
                        xsw.writeEndElement();
                    }
        
                    xsw.writeStartElement("image");
                    xsw.writeAttribute('path', image.path);
                    xsw.writeEndElement();
        
                    xsw.writeEndElement();
                });
            });
            xsw.writeEndElement();
        }
        if (customAttributeIndexs.length > 0) {
            xsw.writeStartElement("custom-attributes");
            customAttributeIndexs.forEach(function (customAttributeIndex) {
                var customAttribute = csvHeader[customAttributeIndex];
                var customAttributeSplit = customAttribute.split(':');
                blankValue = myObjects[customAttributeIndex] || '';
                if (blankValue.trim()) {
                    xsw.writeStartElement('custom-attribute');
                    xsw.writeAttribute('attribute-id', customAttributeSplit[0].substring(2, customAttributeSplit[0].length));
                    if (customAttributeSplit.length === 2) {
                        xsw.writeAttribute('xml:lang', customAttributeSplit[1]);
                    }
                    if (myObjects[customAttributeIndex] && myObjects[customAttributeIndex].split('|').length > 1) {
                        var customAttributeValues = myObjects[customAttributeIndex].split('|');
                        customAttributeValues.forEach(function (value) {
                            xsw.writeStartElement('value');
                            xsw.writeCharacters(StringUtils.encodeString(value, 0).replace(/&amp;/g, "&"));
                            xsw.writeEndElement();
                        });
                    } else {
                        xsw.writeCharacters(myObjects[customAttributeIndex] ? StringUtils.encodeString(myObjects[customAttributeIndex], 0).replace(/&amp;/g, "&") : '');
                    }
                    xsw.writeEndElement();
                }
            });
            xsw.writeEndElement();
        }
        if (variationIndexs.length > 0) {
            xsw.writeStartElement("variations");
            xsw.writeStartElement("attributes");
            variationIndexs.forEach(function (variationIndex) {
                blankValue = myObjects[variationIndex] || '';
                if (csvHeader[variationIndex] !== 'v_variants' && blankValue) {
                    xsw.writeStartElement("variation-attribute");
                    xsw.writeAttribute('attribute-id', csvHeader[variationIndex].substring(2, csvHeader[variationIndex].length));
                    xsw.writeAttribute('variation-attribute-id', csvHeader[variationIndex].substring(2, csvHeader[variationIndex].length));
                    xsw.writeStartElement("display-name");
                    xsw.writeCharacters(csvHeader[variationIndex].substring(2, csvHeader[variationIndex].length));
                    xsw.writeEndElement();
                    xsw.writeStartElement("variation-attribute-values");
                    var variationAttributeValues = myObjects[variationIndex] ? myObjects[variationIndex].split(',') : [];
                    variationAttributeValues.forEach(function (variationAttributeValue) {
                        xsw.writeStartElement("variation-attribute-value");
                        xsw.writeAttribute('value', variationAttributeValue);
                        xsw.writeStartElement("display-value");
                        xsw.writeCharacters(variationAttributeValue);
                        xsw.writeEndElement();
                        xsw.writeEndElement();
                    });
                    xsw.writeEndElement();
                    xsw.writeEndElement();
                }
            });
            xsw.writeEndElement();
            variationIndexs.forEach(function (variationIndex) {
                blankValue = myObjects[variationIndex] || '';
                if (csvHeader[variationIndex] === 'v_variants' && blankValue) {
                    xsw.writeStartElement("variants");
                    var variants = myObjects[variationIndex] ? myObjects[variationIndex].split(',') : [];
                    variants.forEach(function (variant) {
                        xsw.writeStartElement("variant");
                        xsw.writeAttribute('product-id', variant);
                        xsw.writeEndElement();
                    });
                    xsw.writeEndElement();
                }
            });
            xsw.writeEndElement();
        }
        if (storeAttributeIndexs.length > 0) {
            xsw.writeStartElement("store-attributes");
            storeAttributeIndexs.forEach(function (storeAttributeIndex) {
                blankValue = myObjects[storeAttributeIndex] || '';
                if (blankValue){
                    xsw.writeStartElement(csvHeader[storeAttributeIndex].substring(2, csvHeader[storeAttributeIndex].length));
                    xsw.writeCharacters(myObjects[storeAttributeIndex] ? StringUtils.encodeString(myObjects[storeAttributeIndex], 0).replace(/&amp;/g, "&") : '');
                    xsw.writeEndElement();
                }
            });
            xsw.writeEndElement();
        }
        xsw.writeEndElement();
        if (categoryIndexs.length > 0) {
            xsw.writeStartElement("category-assignment");
            categoryIndexs.forEach(function (categoryIndex) {
                blankValue = myObjects[categoryIndex] || '';
                if (csvHeader[categoryIndex] === 'category-id' && blankValue) {
                    var categories = myObjects[categoryIndex].split('|');
                    xsw.writeAttribute('category-id', myObjects[categoryIndex]);
                    xsw.writeAttribute('product-id', myObjects[0]);
                }
            });
            if (primaryFlagIndexs.length > 0) {
                primaryFlagIndexs.forEach(function (primaryFlagIndex) {
                    blankValue = myObjects[primaryFlagIndex] || '';
                    if (csvHeader[primaryFlagIndex] === 'primary-flag' && myObjects[primaryFlagIndex] === 'true' && blankValue) {
                        xsw.writeStartElement("primary-flag");
                        xsw.writeCharacters("true");
                        xsw.writeEndElement();
                    }
                });
            }
            xsw.writeEndElement();
        }
    },
    write: function () {
        write++;
    },
    afterStep: function () {
        xsw.writeEndElement();
        xsw.writeEndDocument();
        xsw.close();
        fileWriter.close();
        csvReader.close();
        var importStatus = new Pipelet('ImportCatalog').execute({
            ImportFile: 'catalog/' + fileName,
            ImportMode: importModeType
        });

        var logFilePath = File.IMPEX + '/log/' + importStatus.get('LogFileName');
        var logFile = new File(logFilePath);
        var logContent = '';

        if (logFile.exists()) {
            var logfileReader = new FileReader(logFile);
            var line = '';
            var counter = 0;
            while ((line = logfileReader.readLine()) !== null && counter <= 2000) {
                logContent += line + '\n';
                counter++;
            }

            logfileReader.close();
        }
        var lastFiles = 'lastFiles' in importCustomObject.custom && importCustomObject.custom.lastFiles ? JSON.parse(importCustomObject.custom.lastFiles) : [];
        var latestFile = 'latestImpExFile' in importCustomObject.custom && importCustomObject.custom.latestImpExFile ? JSON.parse('[' + importCustomObject.custom.latestImpExFile + ']') : {};
        latestFile[0].Status = 'success';
        latestFile[0].logFile = logContent;
        lastFiles.unshift(latestFile[0]);
        if (lastFiles.length > 5) {
            lastFiles.pop();
        }
        Transaction.wrap(function () {
            importCustomObject.custom.latestImpExFile = '';
            importCustomObject.custom.lastFiles = JSON.stringify(lastFiles);
        });
    }
};
