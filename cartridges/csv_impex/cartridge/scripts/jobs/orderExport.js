'use strict';
var File = require('dw/io/File');
var FileReader = require('dw/io/FileReader');
var FileWriter = require('dw/io/FileWriter');
var CSVStreamWriter = require('dw/io/CSVStreamWriter');
var CustomObjectMgr = require('dw/object/CustomObjectMgr');
var ArrayList = require('dw/util/ArrayList');
var Transaction = require('dw/system/Transaction');
var myObject = [];
var csvHeader = [];
var csvDataAll = [];
var exportCustomObject = CustomObjectMgr.getCustomObject('orderCSVImpex', 'orderImpexCSV');
var orderFile = exportCustomObject.custom.inputFile;
var outputFilePath = exportCustomObject.custom.outputFile;

/**
 * getChildNodes returns xml childs of the arg xml object
 * @param {Object} xmlObject XML Object to get child nodes of
 * @returns {Object} object children
 */
function getChildNodes(xmlObject) {
    var myObjectChildren = [];
    var children = xmlObject.children();
    for (var index = 0; index < children.length(); index++) {
        if (children[index].toString().trim() !== '') {
            myObjectChildren.push(children[index]);
        } else if (children[index].attributes().toString().trim() !== '') {
            myObjectChildren.push(children[index]);
        }
    }
    return myObjectChildren;
}

function processAddress(parentKey, addressNode, csvObject) {
    var addressChildren = getChildNodes(addressNode);
    addressChildren.forEach(function (addressChild) {
        var addressKey = parentKey + '-' + addressChild.localName();
        if (!csvHeader.includes(addressKey)) {
            csvHeader.push(addressKey);
        }
        csvObject[addressKey] = addressChild.text();
    });
}

function processSection(parentKey, sectionNode, csvObject) {
    var sectionChildren = getChildNodes(sectionNode);
    sectionChildren.forEach(function (sectionChild, index) {
        var sectionKey = parentKey + '-' + sectionChild.localName();
        if (sectionChild.hasSimpleContent()) {
            if (!csvHeader.includes(sectionKey)) {
                csvHeader.push(sectionKey);
            }
            csvObject[sectionKey] = sectionChild.text();
        } else {
            processSection(sectionKey, sectionChild, csvObject);
        }
    });
}

function processPayment(paymentNode, csvObject) {
    var paymentChildren = getChildNodes(paymentNode);
    paymentChildren.forEach(function (paymentChild, index) {
        var paymentKey = 'payment-' + index + '-' + paymentChild.localName();
        if (paymentChild.localName() === 'credit-card') {
            var creditCardChildren = getChildNodes(paymentChild);
            creditCardChildren.forEach(function (creditCardChild) {
                var creditCardKey = paymentKey + '-' + creditCardChild.localName();
                if (!csvHeader.includes(creditCardKey)) {
                    csvHeader.push(creditCardKey);
                }
                csvObject[creditCardKey] = creditCardChild.text();
            });
        } else {
            if (!csvHeader.includes(paymentKey)) {
                csvHeader.push(paymentKey);
            }
            csvObject[paymentKey] = paymentChild.text();
        }
    });
}

module.exports = {
    beforeStep: function () {
        var file = new File(orderFile);
        var xmlObject = new ArrayList([]);
        if (file.exists()) {
            var fileReader = new FileReader(file);
            var XMLStreamReader = require('dw/io/XMLStreamReader');
            var xmlReader = new XMLStreamReader(fileReader);
            while (xmlReader.hasNext()) {
                try {
                    if (xmlReader.next()) {
                        if (xmlReader.isStartElement()) {
                            var localElementName = xmlReader.getLocalName();
                            if (localElementName === 'order') {
                                xmlObject.push(xmlReader.readXMLObject());
                            }
                        }
                    }
                } catch (error) {
                    continue;
                }
            }
        }
        myObject = xmlObject.iterator();
    },
    read: function () {
        if (myObject.hasNext()) {
            return myObject.next();
        }
    },
    process: function (myObjects) {
        var csvObject = {};
        csvObject['order-no'] = myObjects.attribute('order-no').toString();
        if (!csvHeader.includes('order-no')) {
            csvHeader.push('order-no');
        }
        var myObjectChildren = getChildNodes(myObjects);
        myObjectChildren.forEach(function (myObjectChild) {
            var key = myObjectChild.localName();
            if (key === 'customer') {
                var customerChildren = getChildNodes(myObjectChild);
                customerChildren.forEach(function (customerChild) {
                    if (customerChild.localName() === 'billing-address') {
                        processAddress('customer-billing-address', customerChild, csvObject);
                    } else {
                        var customerKey = 'customer-' + customerChild.localName();
                        if (!csvHeader.includes(customerKey)) {
                            csvHeader.push(customerKey);
                        }
                        csvObject[customerKey] = customerChild.text();
                    }
                });
            } else if (key === 'status') {
                var statusChildren = getChildNodes(myObjectChild);
                statusChildren.forEach(function (statusChild) {
                    var statusKey = 'status-' + statusChild.localName();
                    if (!csvHeader.includes(statusKey)) {
                        csvHeader.push(statusKey);
                    }
                    csvObject[statusKey] = statusChild.text();
                });
            } else if (key === 'product-lineitems') {
                var productLineItems = getChildNodes(myObjectChild);
                productLineItems.forEach(function (productLineItem, index) {
                    var productLineItemChildren = getChildNodes(productLineItem);
                    productLineItemChildren.forEach(function (itemChild) {
                        var productKey = 'product-' + index + '-' + itemChild.localName();
                        if (!csvHeader.includes(productKey)) {
                            csvHeader.push(productKey);
                        }
                        csvObject[productKey] = itemChild.text();
                    });
                });
            } else if (key === 'shipping-lineitems') {
                var shippingLineItems = getChildNodes(myObjectChild);
                shippingLineItems.forEach(function (shippingLineItem, index) {
                    var shippingLineItemChildren = getChildNodes(shippingLineItem);
                    shippingLineItemChildren.forEach(function (itemChild) {
                        var shippingKey = 'shipping-' + index + '-' + itemChild.localName();
                        if (!csvHeader.includes(shippingKey)) {
                            csvHeader.push(shippingKey);
                        }
                        csvObject[shippingKey] = itemChild.text();
                    });
                });
            } else if (key === 'shipments') {
                var shipments = getChildNodes(myObjectChild);
                shipments.forEach(function (shipment, index) {
                    var shipmentChildren = getChildNodes(shipment);
                    shipmentChildren.forEach(function (shipmentChild) {
                        if (shipmentChild.localName() === 'shipping-address') {
                            processAddress('shipment-' + index + '-shipping-address', shipmentChild, csvObject);
                        } else if (shipmentChild.localName() === 'totals') {
                            processSection('shipment-' + index + '-totals', shipmentChild, csvObject);
                        } else if (shipmentChild.localName() === 'status') {
                            var shipmentStatusChildren = getChildNodes(shipmentChild);
                            shipmentStatusChildren.forEach(function (shipmentStatusChild) {
                                var shipmentStatusKey = 'shipment-' + index + '-status-' + shipmentStatusChild.localName();
                                if (!csvHeader.includes(shipmentStatusKey)) {
                                    csvHeader.push(shipmentStatusKey);
                                }
                                csvObject[shipmentStatusKey] = shipmentStatusChild.text();
                            });
                        } else {
                            var shipmentKey = 'shipment-' + index + '-' + shipmentChild.localName();
                            if (!csvHeader.includes(shipmentKey)) {
                                csvHeader.push(shipmentKey);
                            }
                            csvObject[shipmentKey] = shipmentChild.text();
                        }
                    });
                });
            }  else if (key === 'totals') {
                processSection('total', myObjectChild, csvObject);
            } else if (key === 'payments') {
                var payments = getChildNodes(myObjectChild);
                payments.forEach(function (payment, index) {
                    processPayment(payment, csvObject);
                });
            } else {
                if (!csvHeader.includes(key)) {
                    csvHeader.push(key);
                }
                csvObject[key] = myObjectChild.text();
            }
        });
        return csvObject;
    },
    write: function (csvObjectsList) {
        var csvObjectsIterator = csvObjectsList.iterator();
        while (csvObjectsIterator.hasNext()) {
            var currentObject = csvObjectsIterator.next();
            if (currentObject) {
                var csvData = [];
                csvHeader.forEach(function (prop) {
                    csvData.push(currentObject[prop] ? currentObject[prop] : '');
                });
                csvDataAll.push(csvData);
            }
        }
    },
    afterStep: function () {
        var outputFile = new File(outputFilePath);
        var csvFileWriter = new FileWriter(outputFile);
        var csvStreamWriter = new CSVStreamWriter(csvFileWriter, ',');
        csvStreamWriter.writeNext(csvHeader);
        csvDataAll.forEach(function (csvData) {
            csvStreamWriter.writeNext(csvData);
        });
        csvStreamWriter.close();
        csvFileWriter.close();
        var lastFiles = 'lastFiles' in exportCustomObject.custom && exportCustomObject.custom.lastFiles ? JSON.parse(exportCustomObject.custom.lastFiles) : [];
        var latestFile = 'latestImpExFile' in exportCustomObject.custom && exportCustomObject.custom.latestImpExFile ? JSON.parse('[' + exportCustomObject.custom.latestImpExFile + ']') : {};
        latestFile[0].Status = 'success';
        lastFiles.unshift(latestFile[0]);
        if (lastFiles.length > 5) {
            lastFiles.pop();
        }
        Transaction.wrap(function () {
            exportCustomObject.custom.latestImpExFile = '';
            exportCustomObject.custom.lastFiles = JSON.stringify(lastFiles);
        });
    }
};
