'use strict';

var FileReader = require('dw/io/FileReader');
var FileWriter = require('dw/io/FileWriter');
var XMLStreamWriter = require('dw/io/XMLStreamWriter');
var CSVStreamReader = require('dw/io/CSVStreamReader');
var CustomObjectMgr = require('dw/object/CustomObjectMgr');
var StringUtils = require('dw/util/StringUtils');
var Transaction = require('dw/system/Transaction');
var File = require('dw/io/File');
var Pipelet = require('dw/system/Pipelet');
var importCustomObject = CustomObjectMgr.getCustomObject('catalogCSVImpex', 'catalogImpexCSV');
var inventoryListID = importCustomObject.custom.catalogID;
var CSVFile = importCustomObject.custom.inputFile;
var XMLFile = importCustomObject.custom.outputFile;
var customAttributeIndexs = [];
var systemAttributeIndexs = [];
var storeAttributeIndexs = [];
var imageIndexs = [];
var variationIndexs = [];
var categoryIndexs = [];
var primaryFlagIndexs = [];
var csvIterator;
var csvHeader;
var xmlFile = new File(XMLFile);
var fileWriter = new FileWriter(xmlFile);
var xsw = new XMLStreamWriter(fileWriter);
var file = new File(CSVFile);
var fileName = file.name.replace('.csv', '.xml');
var fileReader = new FileReader(file);
var csvReader = new CSVStreamReader(fileReader);
var write = 0;
module.exports = {
    beforeStep: function () {
        var ProductInventoryMgr = require('dw/catalog/ProductInventoryMgr');
        var inventoryList = ProductInventoryMgr.getInventoryList(inventoryListID);
        var content = csvReader.readAll();
        if (content.length > 0) {
            csvHeader = content[0];
            content.shift();
            csvIterator = content.iterator();
        }

        xsw.writeStartDocument();
        xsw.writeStartElement("inventory");
        xsw.writeAttribute('xmlns', "http://www.demandware.com/xml/impex/inventory/2007-05-31");
        xsw.writeStartElement("inventory-list");
        xsw.writeStartElement("header");
        xsw.writeAttribute('list-id', inventoryList.ID);
        xsw.writeStartElement("default-instock");
        xsw.writeCharacters(inventoryList.defaultInStockFlag);
        xsw.writeEndElement();
        xsw.writeStartElement("description");
        xsw.writeCharacters(inventoryList.description);
        xsw.writeEndElement();
        xsw.writeEndElement();
        xsw.writeStartElement("records");
    },
    read: function () {
        if (csvIterator && csvIterator.hasNext()) {
            return csvIterator.next();
        }
    },
    process: function (myObjects) {
        xsw.writeStartElement("record");
        xsw.writeAttribute('product-id', myObjects[csvHeader.indexOf('product-id')]); // Assuming 'product-id' is one of the headers
        
        var allocationIndex = csvHeader.indexOf('allocation');
        if (allocationIndex !== -1 && myObjects[allocationIndex] > -1) {
            xsw.writeStartElement("allocation");
            xsw.writeCharacters(myObjects[allocationIndex]);
            xsw.writeEndElement();
        } else {
            xsw.writeStartElement("allocation");
            xsw.writeCharacters("0");
            xsw.writeEndElement();
        }

        var perpetualIndex = csvHeader.indexOf('perpetual');
        if (perpetualIndex !== -1 && myObjects[perpetualIndex]) {
            xsw.writeStartElement("perpetual");
            xsw.writeCharacters(myObjects[perpetualIndex]);
            xsw.writeEndElement();
        }

        xsw.writeEndElement(); // Closing record
    },
    write: function () {
        write++;
    },
    afterStep: function () {
        xsw.writeEndElement(); // Closing records
        xsw.writeEndElement(); // Closing inventory-list
        xsw.writeEndElement(); // Closing inventory
        xsw.writeEndDocument();
        xsw.close();
        fileWriter.close();
        csvReader.close();
        var importStatus = new Pipelet('ImportInventoryLists').execute({
            ImportFile: 'catalog/' + fileName,
            ImportMode: 'MERGE'
        });

        var logFilePath = File.IMPEX + '/log/' + importStatus.get('LogFileName');
        var logFile = new File(logFilePath);
        var logContent = '';

        if (logFile.exists()) {
            var logfileReader = new FileReader(logFile);
            var line = '';
            var counter = 0;    
            while ((line = logfileReader.readLine()) !== null && counter <= 2000) {
                logContent += line + '\n';
                counter++;
            }

            logfileReader.close();
        }
        var lastFiles = 'lastFiles' in importCustomObject.custom && importCustomObject.custom.lastFiles ? JSON.parse(importCustomObject.custom.lastFiles) : [];
        var latestFile = 'latestImpExFile' in importCustomObject.custom && importCustomObject.custom.latestImpExFile ? JSON.parse('[' + importCustomObject.custom.latestImpExFile + ']') : {};
        latestFile[0].Status = 'success';
        latestFile[0].logFile = logContent;
        lastFiles.unshift(latestFile[0]);
        if (lastFiles.length > 5) {
            lastFiles.pop();
        }
        Transaction.wrap(function () {
            importCustomObject.custom.latestImpExFile = '';
            importCustomObject.custom.lastFiles = JSON.stringify(lastFiles);
        });
    }
};