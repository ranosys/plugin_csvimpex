'use strict';

var FileReader = require('dw/io/FileReader');
var FileWriter = require('dw/io/FileWriter');
var XMLStreamWriter = require('dw/io/XMLStreamWriter');
var CSVStreamReader = require('dw/io/CSVStreamReader');
var CustomObjectMgr = require('dw/object/CustomObjectMgr');
var StringUtils = require('dw/util/StringUtils');
var Transaction = require('dw/system/Transaction');
var File = require('dw/io/File');
var Pipelet = require('dw/system/Pipelet');
var importCustomObject = CustomObjectMgr.getCustomObject('customerCSVImpex', 'customerImpexCSV');
var CSVFile = importCustomObject.custom.inputFile;
var XMLFile = importCustomObject.custom.outputFile;
var csvIterator;
var csvHeader;
var xmlFile = new File(XMLFile);
var fileWriter = new FileWriter(xmlFile);
var xsw = new XMLStreamWriter(fileWriter);
var file = new File(CSVFile);
var fileName = file.name.replace('.csv', '.xml');
var fileReader = new FileReader(file);
var csvReader = new CSVStreamReader(fileReader);
var write = 0;

module.exports = {
    beforeStep: function () {
        var content = csvReader.readAll();
        if (content.length > 0) {
            csvHeader = content[0];
            content.shift();
            csvIterator = content.iterator();
        }

        xsw.writeStartDocument();
        xsw.writeStartElement("customers");
        xsw.writeAttribute('xmlns', "http://www.demandware.com/xml/impex/customer/2006-10-31");
    },

    read: function () {
        if (csvIterator && csvIterator.hasNext()) {
            return csvIterator.next();
        }
    },

    process: function (myObjects) {
        xsw.writeStartElement("customer");
        xsw.writeAttribute("customer-no", myObjects[csvHeader.indexOf('customer-no')]); // Assuming 'customer-no' is one of the headers

        // Writing credentials
        xsw.writeStartElement("credentials");
        xsw.writeStartElement("login");
        if (myObjects[csvHeader.indexOf('credentials_login')]) {
            xsw.writeCharacters(myObjects[csvHeader.indexOf('credentials_login')]);
        }
        xsw.writeEndElement();

        xsw.writeStartElement("password");
        xsw.writeAttribute('encrypted', "true");
        xsw.writeAttribute('encryptionScheme', "scrypt");
        if (myObjects[csvHeader.indexOf('credentials_password')]) {
            xsw.writeCharacters(myObjects[csvHeader.indexOf('credentials_password')]);
        }
        xsw.writeEndElement();

        xsw.writeStartElement("enabled-flag");
        if (myObjects[csvHeader.indexOf('credentials_enabled-flag')]) {
            xsw.writeCharacters(myObjects[csvHeader.indexOf('credentials_enabled-flag')]);
        }
        xsw.writeEndElement();

        xsw.writeStartElement("password-question");
        xsw.writeEndElement();
        xsw.writeStartElement("password-answer");
        xsw.writeEndElement();
        xsw.writeEndElement(); // Closing credentials

        // Writing profile
        xsw.writeStartElement("profile");
        xsw.writeStartElement("salutation");
        if (myObjects[csvHeader.indexOf('profile_salutation')]) {
            xsw.writeCharacters(myObjects[csvHeader.indexOf('profile_salutation')]);
        }
        xsw.writeEndElement();

        xsw.writeStartElement("title");
        if (myObjects[csvHeader.indexOf('profile_title')]) {
            xsw.writeCharacters(myObjects[csvHeader.indexOf('profile_title')]);
        }
        xsw.writeEndElement();

        xsw.writeStartElement("first-name");
        if (myObjects[csvHeader.indexOf('profile_first-name')]) {
            xsw.writeCharacters(myObjects[csvHeader.indexOf('profile_first-name')]);
        }
        xsw.writeEndElement();

        xsw.writeStartElement("second-name");
        if (myObjects[csvHeader.indexOf('profile_second-name')]) {
            xsw.writeCharacters(myObjects[csvHeader.indexOf('profile_second-name')]);
        }
        xsw.writeEndElement();

        xsw.writeStartElement("last-name");
        if (myObjects[csvHeader.indexOf('profile_last-name')]) {
            xsw.writeCharacters(myObjects[csvHeader.indexOf('profile_last-name')]);
        }
        xsw.writeEndElement();

        xsw.writeStartElement("suffix");
        // No need to write anything for suffix if it's not available in your CSV
        xsw.writeEndElement();

        xsw.writeStartElement("company-name");
        if (myObjects[csvHeader.indexOf('profile_company-name')]) {
            xsw.writeCharacters(myObjects[csvHeader.indexOf('profile_company-name')]);
        }
        xsw.writeEndElement();

        xsw.writeStartElement("job-title");
        if (myObjects[csvHeader.indexOf('profile_job-title')]) {
            xsw.writeCharacters(myObjects[csvHeader.indexOf('profile_job-title')]);
        }
        xsw.writeEndElement();

        xsw.writeStartElement("email");
        if (myObjects[csvHeader.indexOf('profile_email')]) {
            xsw.writeCharacters(myObjects[csvHeader.indexOf('profile_email')]);
        }
        xsw.writeEndElement();

        xsw.writeStartElement("phone-home");
        if (myObjects[csvHeader.indexOf('profile_phone-home')]) {
            xsw.writeCharacters(myObjects[csvHeader.indexOf('profile_phone-home')]);
        }
        xsw.writeEndElement();

        xsw.writeStartElement("phone-business");
        if (myObjects[csvHeader.indexOf('profile_phone-business')]) {
            xsw.writeCharacters(myObjects[csvHeader.indexOf('profile_phone-business')]);
        }
        xsw.writeEndElement();

        xsw.writeStartElement("phone-mobile");
        if (myObjects[csvHeader.indexOf('profile_phone-mobile')]) {
            xsw.writeCharacters(myObjects[csvHeader.indexOf('profile_phone-mobile')]);
        }
        xsw.writeEndElement();

        xsw.writeStartElement("fax");
        if (myObjects[csvHeader.indexOf('profile_fax')]) {
            xsw.writeCharacters(myObjects[csvHeader.indexOf('profile_fax')]);
        }
        xsw.writeEndElement();

        xsw.writeStartElement("birthday");
        if (myObjects[csvHeader.indexOf('profile_birthday')]) {
            xsw.writeCharacters(myObjects[csvHeader.indexOf('profile_birthday')]);
        }
        xsw.writeEndElement();

        xsw.writeStartElement("gender");
        if (myObjects[csvHeader.indexOf('profile_gender')]) {
            xsw.writeCharacters(myObjects[csvHeader.indexOf('profile_gender')]);
        }
        xsw.writeEndElement();

        xsw.writeStartElement("creation-date");
        if (myObjects[csvHeader.indexOf('profile_creation-date')]) {
            xsw.writeCharacters(myObjects[csvHeader.indexOf('profile_creation-date')]);
        }
        xsw.writeEndElement();

        xsw.writeStartElement("last-login-time");
        if (myObjects[csvHeader.indexOf('profile_last-login-time')]) {
            xsw.writeCharacters(myObjects[csvHeader.indexOf('profile_last-login-time')]);
        }
        xsw.writeEndElement();

        xsw.writeStartElement("last-visit-time");
        if (myObjects[csvHeader.indexOf('profile_last-visit-time')]) {
            xsw.writeCharacters(myObjects[csvHeader.indexOf('profile_last-visit-time')]);
        }
        xsw.writeEndElement();

        xsw.writeStartElement("preferred-locale");
        xsw.writeEndElement();

        // Writing custom attributes
        xsw.writeStartElement("custom-attributes");
        csvHeader.forEach(function (header) {
            if (header.startsWith('c_')) {
                var attributeId = header.substring(2);
                xsw.writeStartElement("custom-attribute");
                xsw.writeAttribute('attribute-id', attributeId);
                xsw.writeCharacters(myObjects[csvHeader.indexOf(header)]);
                xsw.writeEndElement(); // Closing custom-attribute
            }
        });
        xsw.writeEndElement(); // Closing custom-attributes

        xsw.writeEndElement(); // Closing profile

        // Writing addresses
        xsw.writeStartElement("addresses");
        xsw.writeStartElement("address");
        xsw.writeAttribute('address-id', myObjects[csvHeader.indexOf('address-id')]); // Assuming 'address-id' is one of the headers
        xsw.writeAttribute('preferred', "true");
        xsw.writeStartElement("salutation");
        if (myObjects[csvHeader.indexOf('address_salutation')]) {
            xsw.writeCharacters(myObjects[csvHeader.indexOf('address_salutation')]);
        }
        xsw.writeEndElement();

        xsw.writeStartElement("title");
        if (myObjects[csvHeader.indexOf('address_title')]) {
            xsw.writeCharacters(myObjects[csvHeader.indexOf('address_title')]);
        }
        xsw.writeEndElement();

        xsw.writeStartElement("first-name");
        if (myObjects[csvHeader.indexOf('address_first-name')]) {
            xsw.writeCharacters(myObjects[csvHeader.indexOf('address_first-name')]);
        }
        xsw.writeEndElement();

        xsw.writeStartElement("second-name");
        if (myObjects[csvHeader.indexOf('address_second-name')]) {
            xsw.writeCharacters(myObjects[csvHeader.indexOf('address_second-name')]);
        }
        xsw.writeEndElement();

        xsw.writeStartElement("last-name");
        if (myObjects[csvHeader.indexOf('address_last-name')]) {
            xsw.writeCharacters(myObjects[csvHeader.indexOf('address_last-name')]);
        }
        xsw.writeEndElement();

        xsw.writeStartElement("suffix");
        if (myObjects[csvHeader.indexOf('address_suffix')]) {
            xsw.writeCharacters(myObjects[csvHeader.indexOf('address_suffix')]);
        }
        xsw.writeEndElement();

        xsw.writeStartElement("company-name");
        if (myObjects[csvHeader.indexOf('address_company-name')]) {
            xsw.writeCharacters(myObjects[csvHeader.indexOf('address_company-name')]);
        }
        xsw.writeEndElement();

        xsw.writeStartElement("job-title");
        // No need to write anything for job-title if it's not available in your CSV
        xsw.writeEndElement();

        xsw.writeStartElement("address1");
        if (myObjects[csvHeader.indexOf('address_address1')]) {
            xsw.writeCharacters(myObjects[csvHeader.indexOf('address_address1')]);
        }
        xsw.writeEndElement();

        xsw.writeStartElement("address2");
        if (myObjects[csvHeader.indexOf('address_address2')]) {
            xsw.writeCharacters(myObjects[csvHeader.indexOf('address_address2')]);
        }
        xsw.writeEndElement();

        xsw.writeStartElement("suite");
        if (myObjects[csvHeader.indexOf('address_suite')]) {
            xsw.writeCharacters(myObjects[csvHeader.indexOf('address_suite')]);
        }
        xsw.writeEndElement();

        xsw.writeStartElement("postbox");
        if (myObjects[csvHeader.indexOf('address_postbox')]) {
            xsw.writeCharacters(myObjects[csvHeader.indexOf('address_postbox')]);
        }
        xsw.writeEndElement();

        xsw.writeStartElement("city");
        if (myObjects[csvHeader.indexOf('address_city')]) {
            xsw.writeCharacters(myObjects[csvHeader.indexOf('address_city')]);
        }
        xsw.writeEndElement();

        xsw.writeStartElement("postal-code");
        if (myObjects[csvHeader.indexOf('address_postal-code')]) {
            xsw.writeCharacters(myObjects[csvHeader.indexOf('address_postal-code')]);
        }
        xsw.writeEndElement();

        xsw.writeStartElement("state-code");
        if (myObjects[csvHeader.indexOf('address_state-code')]) {
            xsw.writeCharacters(myObjects[csvHeader.indexOf('address_state-code')]);
        }
        xsw.writeEndElement();

        xsw.writeStartElement("country-code");
        if (myObjects[csvHeader.indexOf('address_country-code')]) {
            xsw.writeCharacters(myObjects[csvHeader.indexOf('address_country-code')]);
        }
        xsw.writeEndElement();

        xsw.writeStartElement("phone");
        if (myObjects[csvHeader.indexOf('address_phone')]) {
            xsw.writeCharacters(myObjects[csvHeader.indexOf('address_phone')]);
        }
        xsw.writeEndElement();

        xsw.writeEndElement(); // Closing address

        xsw.writeStartElement("note");
        xsw.writeEndElement(); // Closing note

        xsw.writeEndElement(); // Closing customer
        xsw.writeEndElement(); // Closing customers

    },
    write: function () {
        write++;
    },
    afterStep: function () {
        xsw.writeEndDocument();
        xsw.close();
        fileWriter.close();
        csvReader.close();
        var importStatus = new Pipelet('ImportCustomers').execute({
            ImportMode: 'MERGE',
            ImportFile: 'customer/' + fileName
        });

        var logFilePath = File.IMPEX + '/log/' + importStatus.get('LogFileName');
        var logFile = new File(logFilePath);
        var logContent = '';

        if (logFile.exists()) {
            var logfileReader = new FileReader(logFile);
            var line = '';
            var counter = 0;
            while ((line = logfileReader.readLine()) !== null && counter <= 2000) {
                logContent += line + '\n';
                counter++;
            }

            logfileReader.close();
        }
        var lastFiles = 'lastFiles' in importCustomObject.custom && importCustomObject.custom.lastFiles ? JSON.parse(importCustomObject.custom.lastFiles) : [];
        var latestFile = 'latestImpExFile' in importCustomObject.custom && importCustomObject.custom.latestImpExFile ? JSON.parse('[' + importCustomObject.custom.latestImpExFile + ']') : {};
        latestFile[0].Status = 'success';
        latestFile[0].logFile = logContent;
        lastFiles.unshift(latestFile[0]);
        if (lastFiles.length > 5) {
            lastFiles.pop();
        }
        Transaction.wrap(function () {
            importCustomObject.custom.latestImpExFile = '';
            importCustomObject.custom.lastFiles = JSON.stringify(lastFiles);
        });
    }
};
