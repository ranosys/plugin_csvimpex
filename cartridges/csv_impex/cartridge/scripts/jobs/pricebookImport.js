'use strict';

var FileReader = require('dw/io/FileReader');
var FileWriter = require('dw/io/FileWriter');
var XMLStreamWriter = require('dw/io/XMLStreamWriter');
var CSVStreamReader = require('dw/io/CSVStreamReader');
var CustomObjectMgr = require('dw/object/CustomObjectMgr');
var Transaction = require('dw/system/Transaction');
var File = require('dw/io/File');
var Pipelet = require('dw/system/Pipelet');
var importCustomObject = CustomObjectMgr.getCustomObject('catalogCSVImpex', 'catalogImpexCSV');
var pricebookID = importCustomObject.custom.catalogID;
var CSVFile = importCustomObject.custom.inputFile;
var XMLFile = importCustomObject.custom.outputFile;
var csvIterator;
var xmlFile = new File(XMLFile);
var fileWriter = new FileWriter(xmlFile);
var xsw = new XMLStreamWriter(fileWriter);
var file = new File(CSVFile);
var fileName = file.name.replace('.csv', '.xml');
var fileReader = new FileReader(file);
var csvReader = new CSVStreamReader(fileReader);
var write = 0;
module.exports = {
    beforeStep: function () {
        var PriceBookMgr = require('dw/catalog/PriceBookMgr');
        var pricebook = PriceBookMgr.getPriceBook(pricebookID);
        var content = csvReader.readAll();
        xsw.writeStartDocument();
        xsw.writeStartElement("pricebooks");
        xsw.writeAttribute('xmlns', "http://www.demandware.com/xml/impex/pricebook/2006-10-31");
        xsw.writeStartElement("pricebook");
        xsw.writeStartElement("header");
        xsw.writeAttribute('pricebook-id', pricebookID);
        xsw.writeStartElement("currency");
        xsw.writeCharacters(pricebook.currencyCode);
        xsw.writeEndElement();
        xsw.writeStartElement("display-name");
        xsw.writeAttribute('xml:lang', "x-default");
        xsw.writeCharacters(pricebook.displayName);
        xsw.writeEndElement();
        xsw.writeStartElement("online-flag");
        xsw.writeCharacters(pricebook.onlineFlag);
        xsw.writeEndElement();
        if ('parentPriceBook' in pricebook && pricebook.parentPriceBook && 'ID' in pricebook.parentPriceBook) {
            xsw.writeStartElement("parent");
            xsw.writeCharacters(pricebook.parentPriceBook.ID);
            xsw.writeEndElement();
        }
        xsw.writeEndElement();
        xsw.writeStartElement("price-tables");
        content.shift();
        csvIterator = content.iterator();
    },
    read: function () {
        if (csvIterator.hasNext()) {
            return csvIterator.next();
        }
    },
    process: function (myObjects) {
        xsw.writeStartElement("price-table");
        xsw.writeAttribute('product-id', myObjects[0]);
        xsw.writeStartElement("amount");
        xsw.writeAttribute('quantity', myObjects[1]);
        xsw.writeCharacters(myObjects[2]);
        xsw.writeEndElement();
        xsw.writeEndElement();
    },
    write: function () {
        write++;
    },
    afterStep: function () {
        xsw.writeEndElement();
        xsw.writeEndElement();
        xsw.writeEndElement();
        xsw.writeEndDocument();
        xsw.close();
        fileWriter.close();
        csvReader.close();
        var importStatus = new Pipelet('ImportPriceBooks').execute({
            ImportFile: 'catalog/' + fileName,
            ImportMode: 'MERGE'
        });

        var logFilePath = File.IMPEX + '/log/' + importStatus.get('LogFileName');
        var logFile = new File(logFilePath);
        var logContent = '';

        if (logFile.exists()) {
            var logfileReader = new FileReader(logFile);
            var line = '';
            var counter = 0;
            while ((line = logfileReader.readLine()) !== null && counter <= 2000) {
                logContent += line + '\n';
                counter++;
            }

            logfileReader.close();
        }
        var lastFiles = 'lastFiles' in importCustomObject.custom && importCustomObject.custom.lastFiles ? JSON.parse(importCustomObject.custom.lastFiles) : [];
        var latestFile = 'latestImpExFile' in importCustomObject.custom && importCustomObject.custom.latestImpExFile ? JSON.parse('[' + importCustomObject.custom.latestImpExFile + ']') : {};
        latestFile[0].Status = 'success';
        latestFile[0].logFile = logContent;
        lastFiles.unshift(latestFile[0]);
        if (lastFiles.length > 5) {
            lastFiles.pop();
        }
        Transaction.wrap(function () {
            importCustomObject.custom.latestImpExFile = '';
            importCustomObject.custom.lastFiles = JSON.stringify(lastFiles);
        });
    }
};
