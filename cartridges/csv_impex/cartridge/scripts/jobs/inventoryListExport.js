'use strict';
var File = require('dw/io/File');
var FileReader = require('dw/io/FileReader');
var FileWriter = require('dw/io/FileWriter');
var CSVStreamWriter = require('dw/io/CSVStreamWriter');
var CustomObjectMgr = require('dw/object/CustomObjectMgr');
var ArrayList = require('dw/util/ArrayList');
var Transaction = require('dw/system/Transaction');
var myObject = [];
var csvHeader = ['product-id', 'allocation', 'allocation-timestamp', 'perpetual', 'preorder-backorder-handling', 'ats', 'on-order', 'turnover'];
var csvDataAll = [];
var exportCustomObject = CustomObjectMgr.getCustomObject('catalogCSVImpex', 'catalogImpexCSV');
var inventoryFile = exportCustomObject.custom.inputFile;
var outputFilePath = exportCustomObject.custom.outputFile;

/**
 * getChildNodes returns xml childs of the arg xml object
 * @param {Object} xmlObject XML Object to get child nodes of
 * @returns {Object} object children
 */
function getChildNodes(xmlObject) {
    var myObjectChildren = [];
    var children = xmlObject.children();
    for (var index = 0; index < children.length(); index++) {
        if (children[index].toString().trim() !== '') {
            myObjectChildren.push(children[index]);
        } else if (children[index].attributes().toString().trim() !== '') {
            myObjectChildren.push(children[index]);
        }
    }
    return myObjectChildren;
}

module.exports = {
    beforeStep: function () {
        var file = new File(inventoryFile);
        var xmlObject = new ArrayList([]);
        if (file.exists()) {
            // Test start
            var fileReader = new FileReader(file);
            var XMLStreamReader = require('dw/io/XMLStreamReader');
            var xmlReader = new XMLStreamReader(fileReader);
            while (xmlReader.hasNext()) {
                try {
                    if (xmlReader.next()) {
                        if (xmlReader.isStartElement()) {
                            var localElementName = xmlReader.getLocalName();
                            if (localElementName === 'record') {
                                xmlObject.push(xmlReader.readXMLObject());
                            }
                        }
                    }
                } catch (error) {
                    var e = error;
                    continue;
                }
            }
        }
        myObject = xmlObject.iterator();
    },
    read: function () {
        if (myObject.hasNext()) {
            return myObject.next();
        }
    },
    process: function (myObjects) {
        var csvObject = {};
        csvObject['product-id'] = myObjects.attribute('product-id').toString();
        var myObjectChildren = getChildNodes(myObjects);
        myObjectChildren.forEach(function (myObjectChild) {
            if (myObjectChild.localName() === 'allocation') {
                csvObject['allocation'] = myObjectChild.children();
            } else if (myObjectChild.localName() === 'allocation-timestamp') {
                csvObject['allocation-timestamp'] = myObjectChild.children();
            } else if (myObjectChild.localName() === 'perpetual') {
                csvObject['perpetual'] = myObjectChild.children();
            } else if (myObjectChild.localName() === 'preorder-backorder-handling') {
                csvObject['preorder-backorder-handling'] = myObjectChild.children();
            } else if (myObjectChild.localName() === 'ats') {
                csvObject['ats'] = myObjectChild.children();
            } else if (myObjectChild.localName() === 'on-order') {
                csvObject['on-order'] = myObjectChild.children();
            } else if (myObjectChild.localName() === 'turnover') {
                csvObject['turnover'] = myObjectChild.children();
            }
        })
        return csvObject;
    },
    write: function (csvObjectsList) {
        var csvObjectsIterator = csvObjectsList.iterator();
        while (csvObjectsIterator.hasNext()) {
            var currentObject = csvObjectsIterator.next();
            if (currentObject) {
                var csvData = [];
                csvHeader.forEach(function (prop) {
                    csvData.push(currentObject[prop] ? currentObject[prop] : '');
                });
                csvDataAll.push(csvData);
            }
        }
    },
    afterStep: function () {
        var outputFile = new File(outputFilePath);
        var csvFileWriter = new FileWriter(outputFile);
        var csvStreamWriter = new CSVStreamWriter(csvFileWriter, ',');
        csvStreamWriter.writeNext(csvHeader);
        csvDataAll.forEach(function (csvData) {
            csvStreamWriter.writeNext(csvData);
        });
        csvStreamWriter.close();
        csvFileWriter.close();
        var lastFiles = 'lastFiles' in exportCustomObject.custom && exportCustomObject.custom.lastFiles ? JSON.parse(exportCustomObject.custom.lastFiles) : [];
        var latestFile = 'latestImpExFile' in exportCustomObject.custom && exportCustomObject.custom.latestImpExFile ? JSON.parse('[' + exportCustomObject.custom.latestImpExFile + ']') : {};
        latestFile[0].Status = 'success';
        lastFiles.unshift(latestFile[0]);
        if (lastFiles.length > 5) {
            lastFiles.pop();
        }
        Transaction.wrap(function () {
            exportCustomObject.custom.latestImpExFile = '';
            exportCustomObject.custom.lastFiles = JSON.stringify(lastFiles);
        });
    }
};
