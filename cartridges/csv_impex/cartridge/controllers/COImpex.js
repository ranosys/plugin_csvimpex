'use strict';

var ISML = require('dw/template/ISML');
var CustomObjectMgr = require('dw/object/CustomObjectMgr');
var File = require('dw/io/File');
var FileReader = require('dw/io/FileReader');
var CSVStreamWriter = require('dw/io/CSVStreamWriter');
var XMLStreamReader = require('dw/io/XMLStreamReader');
var FileWriter = require('dw/io/FileWriter');
var Pipelet = require('dw/system/Pipelet');

/**
 * Renders Custom object impex operations list page
 */
function ListCustomObject() {
    ISML.renderTemplate('customObject/customObject');
}

/**
 * Renders Custom object import operation page
 */
function ImportCustomObjectsCSVLanding() {
    ISML.renderTemplate('customObject/customObjectImport');
}

/**
 * Renders Custom object Export operation page
 */
function ExportCustomObjectsCSVLanding() {
    ISML.renderTemplate('customObject/customObjectExport');
}

/**
 * Performs Custom object CSV Export operation
 */
function ExportCustomObjectsCSV() {
    var customObjID = request.httpParameterMap.customObjID.value;
    var fileName = request.httpParameterMap.outputFile.value;
    var customObjects;
    try {
        customObjects = CustomObjectMgr.getAllCustomObjects(customObjID);
        var viewData = {};
        var exportStatus = new Pipelet('ExportCustomObjects').execute({
            ExportFile: fileName,
            OverwriteExportFile: true,
            CustomObjects: customObjects
        });
        var logFilePath = File.IMPEX + '/log/' + exportStatus.get('LogFileName');
        var logFile = new File(logFilePath);
        var logContent = '';
        if (logFile.exists()) {
            var logFileReader = new FileReader(logFile);
            var line = '';
            var counter = 0;
            while ((line = logFileReader.readLine()) !== null && counter <= 2000) {
                logContent += line + '\n';
                counter++;
            }
            logFileReader.close();
        }
        var customObjFile = File.IMPEX + File.SEPARATOR + 'src/' + fileName;
        var outputFile = new File(File.IMPEX + '/src/');
        outputFile.mkdirs();
        outputFile = new File(File.IMPEX + '/src/' + fileName + '.csv');
        var csvFileWriter = new FileWriter(outputFile);
        var csvStreamWriter = new CSVStreamWriter(csvFileWriter, ',');
        var file = new File(customObjFile);
        if (file.exists()) {
            var fileReader = new FileReader(file);
            var xmlReader = new XMLStreamReader(fileReader);
            var objects = [];
            while (xmlReader.hasNext()) {
                if (xmlReader.next()) {
                    if (xmlReader.isStartElement()) {
                        var localElementName = xmlReader.getLocalName();
                        if (localElementName === 'custom-object') {
                            var myObject = xmlReader.readXMLObject();
                            var csvObject = {};
                            csvObject[myObject.attribute('type-id')] = myObject.attribute('object-id').toString();
                            var myObjectChildren = [];
                            for (var index = 0; index < myObject.children().length(); index++) {
                                if (myObject.children()[index].trim() !== '') {
                                    myObjectChildren.push(myObject.children()[index])
                                }
                            }
                            myObjectChildren.forEach(function (myObjectChild) {
                                csvObject[myObjectChild.attributes()[0]] = myObjectChild.children() ? myObjectChild.children() : '';
                            });
                            objects.push(csvObject);
                        }
                    }
                }
            }
            var csvHeader = Object.keys(objects[0]);
            csvStreamWriter.writeNext(csvHeader);
            objects.forEach(function (element) {
                var csvData = [];
                csvHeader.forEach(function (prop) {
                    csvData.push(element[prop] ? element[prop] : '');
                });
                csvStreamWriter.writeNext(csvData);
            });
            csvStreamWriter.close();
            csvFileWriter.close();
            var url = '/on/demandware.servlet/webdav/Sites/Impex/src/' + fileName + '.csv';
            viewData = { success: true, url: url, logContent: logContent };
        } else {
            viewData = { success: false, logContent: logContent };
        }
        ISML.renderTemplate('customObject/exportComplete', viewData);
    } catch (error) {
        ISML.renderTemplate('customObject/exportComplete', { success: false, error: true, msg: error });
    }
}

/**
 * Performs Custom object CSV Import operation
 */
function ImportCustomObjectsCSV() {
    var CSVStreamReader = require('dw/io/CSVStreamReader');
    var XMLStreamWriter = require('dw/io/XMLStreamWriter');
    var LinkedHashMap = require('dw/util/LinkedHashMap');
    var params = request.httpParameterMap;
    var uploadFile = new LinkedHashMap(); // callback function = constructs a new LinkedHashMap.
    var closure = function (field, ct, oname) {
        return new File(File.IMPEX + '/src/' + oname);
    };
    uploadFile = params.processMultipart(closure);
    var file = uploadFile.customObjectCSV;
    var fileName = file.name.replace('.csv', '.xml');
    var outputFile = new File(File.IMPEX + '/src/' + fileName);
    var fileReader = new FileReader(file);
    var csvReader = new CSVStreamReader(fileReader);
    var content = csvReader.readAll();
    var objectAttributesLength = Object.keys(content[0]).length;
    var fileWriter = new FileWriter(outputFile);
    var xsw = new XMLStreamWriter(fileWriter);
    xsw.writeStartDocument();
    xsw.writeStartElement('custom-objects');
    xsw.writeAttribute('xmlns', 'http://www.demandware.com/xml/impex/customobject/2006-10-31');
    for (var index = 1; index < content.length; index++) {
        xsw.writeStartElement('custom-object');
        xsw.writeAttribute('type-id', content[0][0]);
        xsw.writeAttribute('object-id', content[index][0]);
        for (var subIndex = 1; subIndex < objectAttributesLength; subIndex++) {
            xsw.writeStartElement('object-attribute');
            xsw.writeAttribute('attribute-id', content[0][subIndex]);
            xsw.writeCharacters(content[index][subIndex]);
            xsw.writeEndElement();
        }
        xsw.writeEndElement();
    }
    xsw.writeEndElement();
    xsw.writeEndDocument();
    xsw.close();
    fileWriter.close();
    csvReader.close();
    var importStatus = new Pipelet('ImportCustomObjects').execute({
        ImportFile: fileName,
        ImportMode: 'MERGE'
    });
    var logFilePath = File.IMPEX + '/log/' + importStatus.get('LogFileName');
    var logFile = new File(logFilePath);
    var logContent = '';

    if (logFile.exists()) {
        var logFileReader = new FileReader(logFile);
        var line = '';
        var counter = 0;
        while ((line = logFileReader.readLine()) !== null && counter <= 2000) {
            logContent += line + '\n';
            counter++;
        }

        logFileReader.close();
    }
    if (importStatus.get('Status').code === 'IMPEX-0') {
        ISML.renderTemplate('customObject/ImportComplete', { success: true, errorMsg: importStatus.get('ErrorMsg'), logContent: logContent });
    } else {
        ISML.renderTemplate('customObject/ImportComplete', { success: false, errorMsg: importStatus.get('ErrorMsg'), logContent: logContent });
    }
}

ListCustomObject.public = true;
ExportCustomObjectsCSV.public = true;
ImportCustomObjectsCSV.public = true;
ImportCustomObjectsCSVLanding.public = true;
ExportCustomObjectsCSVLanding.public = true;
exports.ListCustomObject = ListCustomObject;
exports.ExportCustomObjectsCSV = ExportCustomObjectsCSV;
exports.ImportCustomObjectsCSV = ImportCustomObjectsCSV;
exports.ImportCustomObjectsCSVLanding = ImportCustomObjectsCSVLanding;
exports.ExportCustomObjectsCSVLanding = ExportCustomObjectsCSVLanding;
