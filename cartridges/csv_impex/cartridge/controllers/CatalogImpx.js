/* eslint-disable quotes */
/* eslint-disable no-loop-func */
'use strict';

var ISML = require('dw/template/ISML');
var File = require('dw/io/File');
var FileReader = require('dw/io/FileReader');
var CustomObjectMgr = require('dw/object/CustomObjectMgr');
var Transaction = require('dw/system/Transaction');
var Pipelet = require('dw/system/Pipelet');
var URLUtils = require('dw/web/URLUtils');
var Resource = require('dw/web/Resource');

/**
 * Renders Catalog Impex Page
 */
function ListCatalogImpex() {
    var exportCustomObject = CustomObjectMgr.getCustomObject('catalogCSVImpex', 'catalogImpexCSV');
    var lastFiles = exportCustomObject.custom.lastFiles ? JSON.parse(exportCustomObject.custom.lastFiles) : [];
    var latestFile = exportCustomObject.custom.latestImpExFile ? JSON.parse(exportCustomObject.custom.latestImpExFile) : [];
    ISML.renderTemplate('catalog/catalogImpex', { lastFiles: lastFiles, latestFile: latestFile });
}

/**
 * Renders Catalog Export Page
 */
function ExportCatalogCSVLanding() {
    ISML.renderTemplate('catalog/catalogExportLanding', { type: 'Catalog', actionURL: URLUtils.url('CatalogImpx-ExportCatalogCSV') });
}

/**
 * Renders Pricebook Export Page
 */
function ExportpricebookCSVLanding() {
    ISML.renderTemplate('catalog/catalogExportLanding', { type: 'Pricebook', actionURL: URLUtils.url('CatalogImpx-ExportPricebookCSV') });
}

/**
 * Renders Pricebook Export Page
 */
function ExportInventoryListCSVLanding() {
    ISML.renderTemplate('catalog/catalogExportLanding', { type: 'Inventory List', actionURL: URLUtils.url('CatalogImpx-ExportInventoryListCSV') });
}

/**
 * Renders Catalog Import Page
 */
function ImportCatalogCSVLanding() {
    ISML.renderTemplate('catalog/catalogImportLanding', { type: 'Catalog' });
}
/**
 * Renders pricebook Import Page
 */
function ImportpricebookCSVLanding() {
    ISML.renderTemplate('catalog/catalogImportLanding', { type: 'Pricebook' });
}

/**
 * Renders pricebook Import Page
 */
function ImportInventoryListCSVLanding() {
    ISML.renderTemplate('catalog/catalogImportLanding', { type: 'Inventory List' });
}
/**
 * Renders Catalog Export Status Page
 */
function CatalogImpexProcess() {
    var index = request.httpParameterMap.index ? request.httpParameterMap.index.value : 0;
    var exportCustomObject = CustomObjectMgr.getCustomObject('catalogCSVImpex', 'catalogImpexCSV');
    var fileObjects = exportCustomObject.custom.lastFiles ? exportCustomObject.custom.lastFiles : [];
    var lastFiles = JSON.parse(fileObjects);
    var viewData = {};
    if (lastFiles.length >= index) {
        viewData = lastFiles[index];
    }

    // Check if the file entry does include the string 'import'.
    if (!viewData.type.includes('import')) {
        viewData.fileType = 'EXPORT';
    }
    ISML.renderTemplate('catalog/impexComplete', viewData);
}

/**
 * Imports CSV file Catalog
 */
function ImportCatalogCSV() {
    var LinkedHashMap = require('dw/util/LinkedHashMap');
    var params = request.httpParameterMap;
    var CatalogID = params.catalogID;
    var importType = params.importType.value;
    var uploadFile = new LinkedHashMap();
    if (importType === 'Catalog') {
        var importModeType = params.importModeType.value;
    }
    var closure = function (field, ct, oname) {
        return new File(File.IMPEX + "/src/catalog/" + oname);
    };

    uploadFile = params.processMultipart(closure);
    var file = uploadFile.catalogFile;
    var importCustomObject = CustomObjectMgr.getCustomObject('catalogCSVImpex', 'catalogImpexCSV');
    var fileName = file.name.replace('.csv', '.xml');
    var outputFile = new File(File.IMPEX + '/src/catalog/' + fileName);
    Transaction.wrap(function () {
        importCustomObject.custom.inputFile = file.fullPath;
        importCustomObject.custom.outputFile = outputFile.fullPath;
        importCustomObject.custom.catalogID = CatalogID;
        importCustomObject.custom.importModeType = importType === 'Catalog' ? importModeType : 'MERGE';
    });
    var JobName;
    if (importType === 'Pricebook') {
        JobName = 'pricebookImportCSV';
    } else if (importType === 'Catalog') {
        JobName = 'catalogImportCSV';
    } else if (importType === 'Inventory List') {
        JobName = 'inventoryListImportCSV';
    }
    var jobStatus = new Pipelet('RunJobNow').execute({
        JobName: JobName
    });
    var date = new Date();
    var type;
    if (importType === 'Pricebook') {
        type = 'pricebook import';
    } else if (importType === 'Catalog') {
        type = 'catalog import';
    } else if (importType === 'Inventory List') {
        type = 'inventory list import';
    }
    var latestImpExFile = {
        type: type,
        logFile: '',
        filePath: '/on/demandware.servlet/webdav/Sites/Impex/src/catalog/' + fileName,
        fileName: fileName,
        exportTime: date.toLocaleDateString() + ' ' + date.toLocaleTimeString(),
        Status: 'running'
    };
    Transaction.wrap(function () {
        importCustomObject.custom.latestImpExFile = JSON.stringify(latestImpExFile);
    });
    ISML.renderTemplate('catalog/catalogImpex', {
        lastFiles: importCustomObject.custom.lastFiles ? JSON.parse(importCustomObject.custom.lastFiles) : [],
        latestFile: importCustomObject.custom.latestImpExFile,
        jobStatus: jobStatus,
        importModeType: CatalogID === 'Catalog' ? importCustomObject.custom.importModeType : 'MERGE'
    });
}

/**
 * Exports CSV file Catalog
 */
function ExportCatalogCSV() {
    var CatalogMgr = require('dw/catalog/CatalogMgr');
    var ProductMgr = require('dw/catalog/ProductMgr');
    var ArrayList = require('dw/util/ArrayList');
    var catalogID = request.httpParameterMap.catalogID.value;
    var productIDList = request.httpParameterMap.productID.value; 
    var ProductExportCheckbox = request.httpParameterMap.ProductExportCheckbox;
    var attributesIDs = request.httpParameterMap.attributeID.value;
    var viewData = {};
    var logContent = '';
    var date;
    var Catalog = CatalogMgr.getCatalog(catalogID);
    var Products = new ArrayList();

    if (ProductExportCheckbox.value) {
        if (!productIDList) {
            viewData = {
                success: false,
                logContent: Resource.msg('product.required', 'csv', null)
            };
            ISML.renderTemplate('catalog/catalogImpex', viewData);
            return;
        }

        if (productIDList) {
            var productIDs = productIDList.split(',');
            productIDs.forEach(function (productID) {
                var products = ProductMgr.getProduct(productID);
                if (products) {
                    Products.push(products);
                }
            });
        }
    }

    var fileName = request.httpParameterMap.outputFile.value;
    var exportStatus;
    var exportCustomObject = CustomObjectMgr.getCustomObject('catalogCSVImpex', 'catalogImpexCSV');
    if (Products && ProductExportCheckbox.value) {
        exportStatus = new Pipelet('ExportCatalog').execute({
            Catalog: Catalog,
            ExportFile: 'catalog/' + fileName,
            Products: Products.iterator()
    
        });
    } else {
        exportStatus = new Pipelet('ExportCatalog').execute({
            Catalog: Catalog,
            ExportFile: 'catalog/' + fileName
        });
    }
    var logFilePath = File.IMPEX + '/log/' + exportStatus.get('LogFileName');
    var logFile = new File(logFilePath);
    if (logFile.exists()) {
        var fileReader = new FileReader(logFile);
        var line = '';
        var counter = 0;
        while ((line = fileReader.readLine()) !== null && counter <= 2000) {
            logContent += line + '\n';
            counter++;
        }
        fileReader.close();
    }
    var latestImpExFile;
    if (exportStatus && exportStatus.result === 1) {
        var inputFileName = File.IMPEX + File.SEPARATOR + 'src/catalog/' + fileName;
        var outputFileName = File.IMPEX + '/src/catalog/' + fileName + '.csv';
        var outputFile = new File(File.IMPEX + '/src/catalog/');
        outputFile.mkdirs();
        outputFile = new File(outputFileName);
        // Inside your script controller...
        Transaction.wrap(function () {
            exportCustomObject.custom.inputFile = inputFileName;
            exportCustomObject.custom.outputFile = outputFileName;
            if (attributesIDs) {
                exportCustomObject.custom.catalogAttributes = attributesIDs;
            } else {
                exportCustomObject.custom.catalogAttributes = ' ';
            }
            date = new Date();
            latestImpExFile = {
                type: 'catalog export',
                logFile: logContent,
                filePath: '/on/demandware.servlet/webdav/Sites/Impex/src/catalog/' + fileName + '.csv',
                fileName: fileName + '.csv',
                exportTime: date.toLocaleDateString() + ' ' + date.toLocaleTimeString(),
                Status: 'running'
            };
            exportCustomObject.custom.latestImpExFile = JSON.stringify(latestImpExFile);
        });
        var jobStatus = new Pipelet('RunJobNow').execute({
            JobName: 'catalogExportCSV'
        });
        var url = '/on/demandware.servlet/webdav/Sites/Impex/src/catalog/' + fileName + '.csv';
        viewData = { success: true, url: url, logContent: logContent, jobStatus: jobStatus };
    } else {
        date = new Date();
        latestImpExFile = {
            type: 'catalog export',
            logFile: logContent,
            filePath: '/on/demandware.servlet/webdav/Sites/Impex/src/catalog/' + fileName + '.csv',
            fileName: fileName + '.csv',
            exportTime: date.toLocaleDateString() + ' ' + date.toLocaleTimeString(),
            Status: 'Error'
        };
        var lastFiles = 'lastFiles' in exportCustomObject.custom && exportCustomObject.custom.lastFiles ? JSON.parse(exportCustomObject.custom.lastFiles) : [];
        lastFiles.push(latestImpExFile);
        if (lastFiles.length >= 5) {
            lastFiles.shift();
        }
        Transaction.wrap(function () {
            exportCustomObject.custom.lastFiles = JSON.stringify(lastFiles);
        });
        viewData = { success: false, logContent: logContent };
    }
    viewData.lastFiles = exportCustomObject.custom.lastFiles ? JSON.parse(exportCustomObject.custom.lastFiles) : [];
    viewData.latestFile = exportCustomObject.custom.latestImpExFile;
    ISML.renderTemplate('catalog/catalogImpex', viewData);
}


/**
 * Exports CSV file Pricebook
 */
function ExportPricebookCSV() {
    var PriceBookMgr = require('dw/catalog/PriceBookMgr');
    var ArrayList = require('dw/util/ArrayList');
    var pricebookID = request.httpParameterMap.catalogID.value;
    var viewData = {};
    var Pricebook = PriceBookMgr.getPriceBook(pricebookID);
    var pricebooks = new ArrayList();
    pricebooks.push(Pricebook);
    var fileName = request.httpParameterMap.outputFile.value;
    var exportCustomObject = CustomObjectMgr.getCustomObject('catalogCSVImpex', 'catalogImpexCSV');
    var exportStatus = new Pipelet('ExportPriceBooks').execute({
        PriceBooks: pricebooks.iterator(),
        ExportFile: 'catalog/' + fileName
    });
    var logFilePath = File.IMPEX + '/log/' + exportStatus.get('LogFileName');
    var logFile = new File(logFilePath);
    var logContent = '';
    if (logFile.exists()) {
        var fileReader = new FileReader(logFile);
        var line = '';
        var counter = 0;
        while ((line = fileReader.readLine()) !== null && counter <= 2000) {
            logContent += line + '\n';
            counter++;
        }
        fileReader.close();
    }
    var latestImpExFile;
    if (exportStatus && exportStatus.result === 1) {
        var inputFileName = File.IMPEX + File.SEPARATOR + 'src/catalog/' + fileName;
        var outputFileName = File.IMPEX + '/src/catalog/' + fileName + '.csv';
        var outputFile = new File(File.IMPEX + '/src/catalog/');
        outputFile.mkdirs();
        outputFile = new File(outputFileName);
        // Inside your script controller...
        Transaction.wrap(function () {
            exportCustomObject.custom.inputFile = inputFileName;
            exportCustomObject.custom.outputFile = outputFileName;
            var date = new Date();
            latestImpExFile = {
                type: 'pricebook export',
                logFile: logContent,
                filePath: '/on/demandware.servlet/webdav/Sites/Impex/src/catalog/' + fileName + '.csv',
                fileName: fileName + '.csv',
                exportTime: date.toLocaleDateString() + ' ' + date.toLocaleTimeString(),
                Status: 'running'
            };
            exportCustomObject.custom.latestImpExFile = JSON.stringify(latestImpExFile);
        });
        var jobStatus = new Pipelet('RunJobNow').execute({
            JobName: 'pricebookExportCSV'
        });
        var url = '/on/demandware.servlet/webdav/Sites/Impex/src/catalog/' + fileName + '.csv';
        viewData = { success: true, url: url, logContent: logContent, jobStatus: jobStatus };
    } else {
        var date = new Date();
        latestImpExFile = {
            type: 'pricebook export',
            logFile: logContent,
            filePath: '/on/demandware.servlet/webdav/Sites/Impex/src/catalog/' + fileName + '.csv',
            fileName: fileName + '.csv',
            exportTime: date.toLocaleDateString() + ' ' + date.toLocaleTimeString(),
            Status: 'Error'
        };
        var lastFiles = 'lastFiles' in exportCustomObject.custom && exportCustomObject.custom.lastFiles ? JSON.parse(exportCustomObject.custom.lastFiles) : [];
        lastFiles.push(latestImpExFile);
        if (lastFiles.length >= 5) {
            lastFiles.shift();
        }
        Transaction.wrap(function () {
            exportCustomObject.custom.lastFiles = JSON.stringify(lastFiles);
        });
        viewData = { success: false, logContent: logContent };
    }
    viewData.lastFiles = exportCustomObject.custom.lastFiles ? JSON.parse(exportCustomObject.custom.lastFiles) : [];
    viewData.latestFile = exportCustomObject.custom.latestImpExFile;
    ISML.renderTemplate('catalog/catalogImpex', viewData);
}

/**
 * Exports CSV file Inventory List
 */
function ExportInventoryListCSV() {
    var ProductInventoryMgr = require('dw/catalog/ProductInventoryMgr');
    var ArrayList = require('dw/util/ArrayList');
    var inventoryListID = request.httpParameterMap.catalogID.value;
    var viewData = {};
    var InventoryList = ProductInventoryMgr.getInventoryList(inventoryListID);
    var inventoryLists = new ArrayList();
    inventoryLists.push(InventoryList);
    var fileName = request.httpParameterMap.outputFile.value;
    var exportCustomObject = CustomObjectMgr.getCustomObject('catalogCSVImpex', 'catalogImpexCSV');
    var exportStatus = new Pipelet('ExportInventoryLists').execute({
        InventoryLists: inventoryLists.iterator(),
        ExportFile: 'catalog/' + fileName
    });
    var logFilePath = File.IMPEX + '/log/' + exportStatus.get('LogFileName');
    var logFile = new File(logFilePath);
    var logContent = '';
    if (logFile.exists()) {
        var fileReader = new FileReader(logFile);
        var line = '';
        var counter = 0;
        while ((line = fileReader.readLine()) !== null && counter <= 2000) {
            logContent += line + '\n';
            counter++;
        }
        fileReader.close();
    }
    var latestImpExFile;
    if (exportStatus && exportStatus.result === 1) {
        var inputFileName = File.IMPEX + File.SEPARATOR + 'src/catalog/' + fileName;
        var outputFileName = File.IMPEX + '/src/catalog/' + fileName + '.csv';
        var outputFile = new File(File.IMPEX + '/src/catalog/');
        outputFile.mkdirs();
        outputFile = new File(outputFileName);
        // Inside your script controller...
        Transaction.wrap(function () {
            exportCustomObject.custom.inputFile = inputFileName;
            exportCustomObject.custom.outputFile = outputFileName;
            var date = new Date();
            latestImpExFile = {
                type: 'inventoryList export',
                logFile: logContent,
                filePath: '/on/demandware.servlet/webdav/Sites/Impex/src/catalog/' + fileName + '.csv',
                fileName: fileName + '.csv',
                exportTime: date.toLocaleDateString() + ' ' + date.toLocaleTimeString(),
                Status: 'running'
            };
            exportCustomObject.custom.latestImpExFile = JSON.stringify(latestImpExFile);
        });
        var jobStatus = new Pipelet('RunJobNow').execute({
            JobName: 'inventoryListExportCSV'
        });
        var url = '/on/demandware.servlet/webdav/Sites/Impex/src/catalog/' + fileName + '.csv';
        viewData = { success: true, url: url, logContent: logContent, jobStatus: jobStatus };
    } else {
        var date = new Date();
        latestImpExFile = {
            type: 'inventoryList export',
            logFile: logContent,
            filePath: '/on/demandware.servlet/webdav/Sites/Impex/src/catalog/' + fileName + '.csv',
            fileName: fileName + '.csv',
            exportTime: date.toLocaleDateString() + ' ' + date.toLocaleTimeString(),
            Status: 'Error'
        };
        var lastFiles = 'lastFiles' in exportCustomObject.custom && exportCustomObject.custom.lastFiles ? JSON.parse(exportCustomObject.custom.lastFiles) : [];
        lastFiles.push(latestImpExFile);
        if (lastFiles.length >= 5) {
            lastFiles.shift();
        }
        Transaction.wrap(function () {
            exportCustomObject.custom.lastFiles = JSON.stringify(lastFiles);
        });
        viewData = { success: false, logContent: logContent };
    }
    viewData.lastFiles = exportCustomObject.custom.lastFiles ? JSON.parse(exportCustomObject.custom.lastFiles) : [];
    viewData.latestFile = exportCustomObject.custom.latestImpExFile;
    ISML.renderTemplate('catalog/catalogImpex', viewData);
}

ImportCatalogCSV.public = true;
ListCatalogImpex.public = true;
ExportCatalogCSV.public = true;
ExportCatalogCSVLanding.public = true;
ImportCatalogCSVLanding.public = true;
ImportpricebookCSVLanding.public = true;
ImportInventoryListCSVLanding.public = true;
ExportpricebookCSVLanding.public = true;
ExportInventoryListCSVLanding.public = true;
CatalogImpexProcess.public = true;
ExportPricebookCSV.public = true;
ExportInventoryListCSV.public = true;
exports.ExportCatalogCSVLanding = ExportCatalogCSVLanding;
exports.ImportCatalogCSVLanding = ImportCatalogCSVLanding;
exports.ExportPricebookCSV = ExportPricebookCSV;
exports.ExportInventoryListCSV = ExportInventoryListCSV;
exports.ExportpricebookCSVLanding = ExportpricebookCSVLanding;
exports.ImportpricebookCSVLanding = ImportpricebookCSVLanding;
exports.ImportInventoryListCSVLanding = ImportInventoryListCSVLanding;
exports.ExportInventoryListCSVLanding = ExportInventoryListCSVLanding;
exports.ListCatalogImpex = ListCatalogImpex;
exports.ImportCatalogCSV = ImportCatalogCSV;
exports.ExportCatalogCSV = ExportCatalogCSV;
exports.CatalogImpexProcess = CatalogImpexProcess;

