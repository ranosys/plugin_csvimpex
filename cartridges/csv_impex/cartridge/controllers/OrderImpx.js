/* eslint-disable quotes */
/* eslint-disable no-loop-func */
'use strict';

var ISML = require('dw/template/ISML');
var File = require('dw/io/File');
var FileReader = require('dw/io/FileReader');
var CustomObjectMgr = require('dw/object/CustomObjectMgr');
var Transaction = require('dw/system/Transaction');
var Pipelet = require('dw/system/Pipelet');
var URLUtils = require('dw/web/URLUtils');

/**
 * Renders Order Impex Page
 */
function ListOrderImpex() {
    var exportCustomObject = CustomObjectMgr.getCustomObject('orderCSVImpex', 'orderImpexCSV');
    var lastFiles = exportCustomObject.custom.lastFiles ? JSON.parse(exportCustomObject.custom.lastFiles) : [];
    var latestFile = exportCustomObject.custom.latestImpExFile ? JSON.parse(exportCustomObject.custom.latestImpExFile) : [];
    ISML.renderTemplate('order/orderImpex', { lastFiles: lastFiles, latestFile: latestFile });
}


/**
 * Renders Order Export Page
 */
function ExportOrderCSVLanding() {
    ISML.renderTemplate('order/orderExportLanding', { type: 'Order', actionURL: URLUtils.url('OrderImpx-ExportOrderCSV') });
}


/**
 * Renders Order Export Status Page
 */
function OrderImpexProcess() {
    var index = request.httpParameterMap.index ? request.httpParameterMap.index.value : 0;
    var exportCustomObject = CustomObjectMgr.getCustomObject('orderCSVImpex', 'orderImpexCSV');
    var fileObjects = exportCustomObject.custom.lastFiles ? exportCustomObject.custom.lastFiles : [];
    var lastFiles = JSON.parse(fileObjects);
    var viewData = {};
    if (lastFiles.length >= index) {
        viewData = lastFiles[index];
    }
    // Check if the file entry does include the string 'import'.
    if (!viewData.type.includes('import')) {
        viewData.fileType = 'EXPORT';
    }
    ISML.renderTemplate('order/impexComplete', viewData);
}

/**
 * Exports CSV file Order
 */
function ExportOrderCSV() {
    var OrderMgr = require('dw/order/OrderMgr');
    var ArrayList = require('dw/util/ArrayList');
    var orderIDsParam = request.httpParameterMap.orderID.value;
    var viewData = {};
    var orderIDs = orderIDsParam.split(',');
    var orders = new ArrayList();
    orderIDs.forEach(function (orderID) {
        var Order = OrderMgr.getOrder(orderID);
        if (Order) {
            orders.push(Order);
        }
    });

    var fileName = request.httpParameterMap.outputFile.value;
    var exportCustomObject = CustomObjectMgr.getCustomObject('orderCSVImpex', 'orderImpexCSV');
    var exportStatus = new Pipelet('ExportOrders').execute({
        Orders: orders.iterator(),
        ExportFile: 'order/' + fileName
    });
    var logFilePath = File.IMPEX + '/log/' + exportStatus.get('LogFileName');
    var logFile = new File(logFilePath);
    var logContent = '';
    if (logFile.exists()) {
        var fileReader = new FileReader(logFile);
        var line = '';
        var counter = 0;
        while ((line = fileReader.readLine()) !== null && counter <= 2000) {
            logContent += line + '\n';
            counter++;
        }
        fileReader.close();
    }
    var latestImpExFile;
    if (exportStatus && exportStatus.result === 1) {
        var inputFileName = File.IMPEX + File.SEPARATOR + 'src/order/' + fileName;
        var outputFileName = File.IMPEX + '/src/order/' + fileName + '.csv';
        var outputFile = new File(File.IMPEX + '/src/order/');
        outputFile.mkdirs();
        outputFile = new File(outputFileName);
        Transaction.wrap(function () {
            exportCustomObject.custom.inputFile = inputFileName;
            exportCustomObject.custom.outputFile = outputFileName;
            var date = new Date();
            latestImpExFile = {
                type: 'order export',
                logFile: logContent,
                filePath: '/on/demandware.servlet/webdav/Sites/Impex/src/order/' + fileName + '.csv',
                fileName: fileName + '.csv',
                exportTime: date.toLocaleDateString() + ' ' + date.toLocaleTimeString(),
                Status: 'running'
            };
            exportCustomObject.custom.latestImpExFile = JSON.stringify(latestImpExFile);
        });
        var jobStatus = new Pipelet('RunJobNow').execute({
            JobName: 'orderExportCSV'
        });
        var url = '/on/demandware.servlet/webdav/Sites/Impex/src/order/' + fileName + '.csv';
        viewData = { success: true, url: url, logContent: logContent, jobStatus: jobStatus };
    } else {
        var date = new Date();
        latestImpExFile = {
            type: 'order export',
            logFile: logContent,
            filePath: '/on/demandware.servlet/webdav/Sites/Impex/src/order/' + fileName + '.csv',
            fileName: fileName + '.csv',
            exportTime: date.toLocaleDateString() + ' ' + date.toLocaleTimeString(),
            Status: 'Error'
        };
        var lastFiles = 'lastFiles' in exportCustomObject.custom && exportCustomObject.custom.lastFiles ? JSON.parse(exportCustomObject.custom.lastFiles) : [];
        lastFiles.push(latestImpExFile);
        if (lastFiles.length >= 5) {
            lastFiles.shift();
        }
        Transaction.wrap(function () {
            exportCustomObject.custom.lastFiles = JSON.stringify(lastFiles);
        });
        viewData = { success: false, logContent: logContent };
    }
    viewData.lastFiles = exportCustomObject.custom.lastFiles ? JSON.parse(exportCustomObject.custom.lastFiles) : [];
    viewData.latestFile = exportCustomObject.custom.latestImpExFile;
    ISML.renderTemplate('order/orderImpex', viewData);
}

ListOrderImpex.public = true;
OrderImpexProcess.public = true;
ExportOrderCSV.public = true;
ExportOrderCSVLanding.public = true;
exports.ListOrderImpex = ListOrderImpex;
exports.OrderImpexProcess = OrderImpexProcess;
exports.ExportOrderCSV = ExportOrderCSV;
exports.ExportOrderCSVLanding = ExportOrderCSVLanding;
