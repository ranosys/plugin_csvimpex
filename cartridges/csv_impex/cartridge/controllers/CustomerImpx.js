/* eslint-disable quotes */
/* eslint-disable no-loop-func */
'use strict';

var ISML = require('dw/template/ISML');
var File = require('dw/io/File');
var FileReader = require('dw/io/FileReader');
var CustomObjectMgr = require('dw/object/CustomObjectMgr');
var Transaction = require('dw/system/Transaction');
var Pipelet = require('dw/system/Pipelet');
var URLUtils = require('dw/web/URLUtils');

/**
 * Renders Customer Impex Page
 */
function ListCustomerImpex() {
    var exportCustomObject = CustomObjectMgr.getCustomObject('customerCSVImpex', 'customerImpexCSV');
    var lastFiles = exportCustomObject.custom.lastFiles ? JSON.parse(exportCustomObject.custom.lastFiles) : [];
    var latestFile = exportCustomObject.custom.latestImpExFile ? JSON.parse(exportCustomObject.custom.latestImpExFile) : [];
    ISML.renderTemplate('customer/customerImpex', { lastFiles: lastFiles, latestFile: latestFile });
}


/**
 * Renders Customer Export Page
 */
function ExportCustomerCSVLanding() {
    ISML.renderTemplate('customer/customerExportLanding', { type: 'Customer', actionURL: URLUtils.url('CustomerImpx-ExportCustomerCSV') });
}

/**
 * Renders Customer Import Page
 */
function ImportCustomerCSVLanding() {
    ISML.renderTemplate('customer/customerImportLanding', { type: 'Customer' });
}

/**
 * Renders Customer Export Status Page
 */
function CustomerImpexProcess() {
    var index = request.httpParameterMap.index ? request.httpParameterMap.index.value : 0;
    var exportCustomObject = CustomObjectMgr.getCustomObject('customerCSVImpex', 'customerImpexCSV');
    var fileObjects = exportCustomObject.custom.lastFiles ? exportCustomObject.custom.lastFiles : [];
    var lastFiles = JSON.parse(fileObjects);
    var viewData = {};
    if (lastFiles.length >= index) {
        viewData = lastFiles[index];
    }
    // Check if the file entry does include the string 'import'.
    if (!viewData.type.includes('import')) {
        viewData.fileType = 'EXPORT';
    }
    ISML.renderTemplate('customer/impexComplete', viewData);
}

/**
 * Imports CSV file Customer
 */
function ImportCustomerCSV() {
    var LinkedHashMap = require('dw/util/LinkedHashMap');
    var params = request.httpParameterMap;
    var importType = params.importType.value;
    var uploadFile = new LinkedHashMap();
    var closure = function (field, ct, oname) {
        return new File(File.IMPEX + "/src/customer/" + oname);
    };

    uploadFile = params.processMultipart(closure);
    var file = uploadFile.customerFile;
    var importCustomObject = CustomObjectMgr.getCustomObject('customerCSVImpex', 'customerImpexCSV');
    var fileName = file.name.replace('.csv', '.xml');
    var outputFile = new File(File.IMPEX + '/src/customer/' + fileName);
    Transaction.wrap(function () {
        importCustomObject.custom.inputFile = file.fullPath;
        importCustomObject.custom.outputFile = outputFile.fullPath;
    });
    var JobName;
    if (importType === 'Customer') {
        JobName = 'customerImportCSV';
    } 
    var jobStatus = new Pipelet('RunJobNow').execute({
        JobName: JobName
    });
    var date = new Date();
    var type;
    if (importType === 'Customer') {
        type = 'customer import';
    }
    var latestImpExFile = {
        type: type,
        logFile: '',
        filePath: '/on/demandware.servlet/webdav/Sites/Impex/src/customer/' + fileName,
        fileName: fileName,
        exportTime: date.toLocaleDateString() + ' ' + date.toLocaleTimeString(),
        Status: 'running'
    };
    Transaction.wrap(function () {
        importCustomObject.custom.latestImpExFile = JSON.stringify(latestImpExFile);
    });
    ISML.renderTemplate('customer/customerImpex', {
        lastFiles: importCustomObject.custom.lastFiles ? JSON.parse(importCustomObject.custom.lastFiles) : [],
        latestFile: importCustomObject.custom.latestImpExFile,
        jobStatus: jobStatus
    });
}

/**
 * Exports CSV file Customer
 */
function ExportCustomerCSV() {
    var CustomerMgr = require('dw/customer/CustomerMgr');
    var ArrayList = require('dw/util/ArrayList');
    var customerIDsParam = request.httpParameterMap.customerID.value;
    var viewData = {};
    var customerIDs = customerIDsParam.split(',');
    var customers = new ArrayList();
    customerIDs.forEach(function (customerID) {
        var Customer = CustomerMgr.getCustomerByCustomerNumber(customerID);
        if (Customer) {
            customers.push(Customer);
        }
    });

    var fileName = request.httpParameterMap.outputFile.value;
    var exportCustomObject = CustomObjectMgr.getCustomObject('customerCSVImpex', 'customerImpexCSV');
    var exportStatus = new Pipelet('ExportCustomers').execute({
        Customers: customers.iterator(),
        ExportFile: 'customer/' + fileName
    });
    var logFilePath = File.IMPEX + '/log/' + exportStatus.get('LogFileName');
    var logFile = new File(logFilePath);
    var logContent = '';
    if (logFile.exists()) {
        var fileReader = new FileReader(logFile);
        var line = '';
        var counter = 0;
        while ((line = fileReader.readLine()) !== null && counter <= 2000) {
            logContent += line + '\n';
            counter++;
        }
        fileReader.close();
    }
    var latestImpExFile;
    if (exportStatus && exportStatus.result === 1) {
        var inputFileName = File.IMPEX + File.SEPARATOR + 'src/customer/' + fileName;
        var outputFileName = File.IMPEX + '/src/customer/' + fileName + '.csv';
        var outputFile = new File(File.IMPEX + '/src/customer/');
        outputFile.mkdirs();
        outputFile = new File(outputFileName);
        // Inside your script controller...
        Transaction.wrap(function () {
            exportCustomObject.custom.inputFile = inputFileName;
            exportCustomObject.custom.outputFile = outputFileName;
            var date = new Date();
            latestImpExFile = {
                type: 'customer export',
                logFile: logContent,
                filePath: '/on/demandware.servlet/webdav/Sites/Impex/src/customer/' + fileName + '.csv',
                fileName: fileName + '.csv',
                exportTime: date.toLocaleDateString() + ' ' + date.toLocaleTimeString(),
                Status: 'running'
            };
            exportCustomObject.custom.latestImpExFile = JSON.stringify(latestImpExFile);
        });
        var jobStatus = new Pipelet('RunJobNow').execute({
            JobName: 'customerExportCSV'
        });
        var url = '/on/demandware.servlet/webdav/Sites/Impex/src/customer/' + fileName + '.csv';
        viewData = { success: true, url: url, logContent: logContent, jobStatus: jobStatus };
    } else {
        var date = new Date();
        latestImpExFile = {
            type: 'customer export',
            logFile: logContent,
            filePath: '/on/demandware.servlet/webdav/Sites/Impex/src/customer/' + fileName + '.csv',
            fileName: fileName + '.csv',
            exportTime: date.toLocaleDateString() + ' ' + date.toLocaleTimeString(),
            Status: 'Error'
        };
        var lastFiles = 'lastFiles' in exportCustomObject.custom && exportCustomObject.custom.lastFiles ? JSON.parse(exportCustomObject.custom.lastFiles) : [];
        lastFiles.push(latestImpExFile);
        if (lastFiles.length >= 5) {
            lastFiles.shift();
        }
        Transaction.wrap(function () {
            exportCustomObject.custom.lastFiles = JSON.stringify(lastFiles);
        });
        viewData = { success: false, logContent: logContent };
    }
    viewData.lastFiles = exportCustomObject.custom.lastFiles ? JSON.parse(exportCustomObject.custom.lastFiles) : [];
    viewData.latestFile = exportCustomObject.custom.latestImpExFile;
    ISML.renderTemplate('customer/customerImpex', viewData);
}

ListCustomerImpex.public = true;
CustomerImpexProcess.public = true;
ExportCustomerCSV.public = true;
ExportCustomerCSVLanding.public = true;
ImportCustomerCSVLanding.public = true;
ImportCustomerCSV.public = true;
exports.ListCustomerImpex = ListCustomerImpex;
exports.CustomerImpexProcess = CustomerImpexProcess;
exports.ImportCustomerCSV = ImportCustomerCSV;
exports.ExportCustomerCSV = ExportCustomerCSV;
exports.ExportCustomerCSVLanding = ExportCustomerCSVLanding;
exports.ImportCustomerCSVLanding = ImportCustomerCSVLanding;
