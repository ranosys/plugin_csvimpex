# Getting Started

## CARTRIDGE OVERVIEW

 The CSV Import Export cartridge provides a user-friendly interface within Salesforce Commerce Cloud (SFCC) Business Manager for seamless import and export of catalog products, inventory, pricebook, customers, orders, and custom objects.

 ## KEY FEATURES

* CSV Import/Export (Catalog Products, specific products, Inventory, and PriceBooks): Allows users toefficiently manage product data, inventory, and pricing information through CSV files. This feature supports both standard and custom product attributes, enabling comprehensive bulk operations.
    * Catalog Products: Import and export entire product catalogs, including all necessary attributes for bulk creation and updates.
    * Specific Products: Target specific products for selective import/export, allowing for precise updates without altering the whole catalog.
    * Inventory: Manage stock levels and warehouse data through CSV files, ensuring accurate and up-to-date inventory records.
    * PriceBooks: Import and export detailed pricing information, supporting various pricing structures, currencies, and regions.
* CSV Import/Export (Custom Objects): Extends import functionality to custom objects defined within the SFCC instance, providing flexibility in data management and enabling users to extract data from custom objects into CSV format, supporting data backup or external use.
* CSV Import/Export (Customers): It allows users to directly import and export customer data using CSV files. This feature supports bulk operations, making it efficient to manage large volumes of customer information.
* CSV Export (Orders): Allows users to export orders into CSV format.

## INSTALLATION GUIDE

* Clone the cartridge into your project folder

* Change directory to the cartridge  folder and install node module depedencies in cartridge directory

    ```Shell
    cd plugin_CSVimpex

    npm install

* Upload the cartridge to your active code version

* Add the Cartridge to your BM cartridge path under Administration > Sites > Manage Sites > Business Manager - Settings

* Import the metadata provided in the “meta” folder in the root directory of the cartridge.

* Update BM roles permissions to allow the users to access the functionality of CSV import Export.

## HOW TO USE

Once you have completed the cartridge installation steps, you will see a new option titled 'CSV Import &
Export' in the Merchant Tools section. This option will be available under both Custom Objects  Custom Objects, Customers, Ordering  and Products & Catalogs

### Custom Object Import Export

* Go to Merchant Tools > Custom Objects > CSV Import & Export

* You will see the import/export screen, here you can choose the import or export operation you want to perform

* #### Export Custom Objects
    * To export custom objects in csv format, click on the export button and it will take you to the export page.

    * On the export page, there will be two inputs, in the first one you need to key in the custom object ID, and in the second one you need to name the export file.

    * After you click on the export button, you will see the final screen showing the success or error logs and the export csv file to be downloaded.

* #### Import Custom Objects
    * To import custom objects in csv format, click on the import button and it will take you to the import page.

    * Here upload the properly formatted csv file (Check the CSV format section) you want to import and click on the import button,  it will take you to the final screen showing the success or error logs

### Catalog Import Export

* Go to Merchant Tools > Products and Catalogs > CSV Import & Export

* You will see the import/export screen, here you can choose the import or export operation you want to perform and the last 5 import & export operations.

* Here you can perform new import or export operations or check the status of previous operations and download the previously exported files.

* #### Export Catalog

    * To export catalog products in csv format, click on the export button and it will take you to the export page

    * On the export page, please follow these steps:
        * In the first input field, enter the catalog ID.
        * In the second input field, provide a name for the export file.
        * If you want to export specific products:
            * Enter the product IDs in the third input field.
            * Select the corresponding checkbox.
        * If you want to export specific catalog attributes, enter the desired attribute names in the fourth input field.

    * After you click the export button, you will be redirected to the first import/export screen where you can see your operation in the list and its status.

    * Once the status is updated from running to success or error, you can click on the operation to check the logs and download the export file.

* #### Import Catalog

    * To import the catalog from a csv file, click on the import button and it will take you to the import page.

    * On the import page, there will be two inputs, in the first one you need to key in the catalog ID, and in the second one you need to upload/choose the properly formatted import file (Check the CSV format section).

    * Select the merge import mode. Modes define what happens to the data at import. These are the modes: Merge, Update, Replace, and delete.

    * After you click the import button, you will be redirected to the first import/export screen where you can see your operation in the list and its status.

    * Once the status is updated from running to success or error, you can click on the operation to check the logs.

* #### Exports Customers

    * On the export page, please follow these steps:
    * In the first input field, enter the customer IDs.
    * In the second input field, provide a name for the export file.
    * After you click the export button, you will be redirected to the first import/export screen where you can see your operation in the list and its status.
    * Once the status is updated from running to success or error, you can click on the operation to check the logs and download the export file.

* #### Imports Customers

    * On the import page, there will be an option to upload/choose the properly formatted import file (Check the CSV format section).
    * After you click the import button, you will be redirected to the first import/export screen where you can see your operation in the list and its status.
    * Once the status is updated from running to success or error, you can click on the operation to check the logs.

* ### Exports Orders

    * On the export page, please follow these steps:
        * In the first input field, enter the order ID. For multiple orders, separate each ID with a comma.
        * In the second input field, provide a name for the export file.
    * After you click the export button, you will be redirected to the first import/export screen where you can see your operation in the list and its status.
    * Once the status is updated from running to success or error, you can click on the operation to check the logs and download the export file.


## CSV Format

### Custom object import file

* Column 1 in the first row should be the custom object ID, and the rows below this should be the key attribute values.

* All columns in the first row from the second column should be the attribute IDs, and the following rows should be their values.

### Catalog import file

* The first row should be the IDs of the attributes of the product system object.

* The attribute IDs should follow the following pattern
    * The system attribute IDs should be entered as it is.

    * The custom attribute IDs should have a “c_” prefix.

    * The localizable system and custom attributes should have a “:x-default” or “:locale id” suffix.

    * The variation attributes should have a “v_” prefix.

    * The Store attributes should have a “s_” prefix.

    * The images should be provided as “image-index”.

* If any product does not have a value for the attribute the column should be kept blank.

* To assign a category to a product, ensure that the `category-id` column is included in the CSV file. If you also want to designate a category as the primary category for that product, you should add the `primary-flag` column to the CSV file.

* To assign a category from a different catalog to a product, the CSV file should include the columns `product-id`, `category-id`, and `primary-flag`.

* If no product in the file has a value for a given attribute, the attribute should be removed from the file.

### Inventory import file

* Row 1 should contain 3 keys productid, allocation, and perpetual
* The following rows should have their respective values

### Customer import file

* The first row should contain the IDs of the attributes of the customer system object.
* The following rows should contain the respective values for each attribute.
* The attribute IDs should follow the following pattern
* The system attribute IDs should be entered as it is.
* The custom attribute IDs should have a “c_” prefix.
* The localizable system and custom attributes should have a “:x-default” or “: locale id” suffix.
* The date format should be yyyy-mm-dd.
* Passwords will not be updated for new customers.
* If a customer does not have a value for an attribute, the corresponding column should be left blank.


## Change logs

* v1.0.0&emsp;|&emsp;2024-03-18&emsp;|&emsp;first release!
* v2.0.0&emsp;|&emsp;2024-09-03&emsp;|&emsp;Second release
    * Inventory Import/Export and category Assignments using CSV
    * Order Export in CSV
    * Customer Import/Export in CSV


## About Author
- My name is Bhupender Pareek, and I'm a Salesforce Commerce Cloud developer with 3 years of experience building e-commerce solutions. I'm passionate about streamlining processes and creating user-friendly tools. Feel free to connect with me on [LinkedIn](https://www.linkedin.com/in/bhupenderpareek/).
